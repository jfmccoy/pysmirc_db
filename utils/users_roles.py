
def implement_users_roles():
    """
    We don't implement these by default, because the webapp
    needs to extend the mixin with stuff for Flask-Security
    -- but they need a default implementation to generate
    migrations, so just build that on the fly.
    """
    from schema import Model, RoleMixin, UserMixin

    print("Creating dummy User/Role ORM")

    class Role(RoleMixin, Model):
        def __repr__(self):
            return f"Role({self.email})"

    class User(UserMixin, Model):
        def has_role(self, name):
            return name in [r.name for r in self.roles]

        def __repr__(self):
            return f"User({self.email})"

    return Role, User

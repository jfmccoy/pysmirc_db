import sqlalchemy as sa
import tempfile
from alembic import command, config
from schema import Model


class DataBase:
    def __init__(self, uri, model_base=Model):
        self.uri = uri
        self.engine = sa.create_engine(self.uri)
        print("Created new database connection")
        self._session = self._make_session()

        if model_base:
            self._init_model_base(model_base)

    def _make_session(self):
        s = sa.orm.Session(bind=self.engine)
        return s

    def _init_model_base(self, model_base):
        class _QueryProperty:
            def __init__(self, db):
                self.db = db

            def __get__(self, obj, cls):
                return self.db.session.query(cls)
        model_base.query = _QueryProperty(self)
        self.Model = model_base

    @property
    def session(self):
        if self._session is None:
            self._session = self._make_session()
        return self._session

    def migrate(self, alembic_dir):
        print(f"Running migrations from {alembic_dir} ...")
        with tempfile.NamedTemporaryFile() as tmp:
            # Create dummy config file
            alembic_cfg = config.Config(tmp.name)
            alembic_cfg.set_main_option(
                'script_location', alembic_dir
            )
            alembic_cfg.set_main_option('sqlalchemy.url', self.uri)

            # Upgrade the database
            command.upgrade(alembic_cfg, 'head')
            # Stamp the database
            command.stamp(alembic_cfg, 'head')
            print("Database migration complete")

    def delete_all(self):
        for table in reversed(self.Model.metadata.sorted_tables):
            self.session.execute(table.delete())
        self.session.commit()

import argparse
import os
from demos import *

ROOT = os.path.abspath(os.path.dirname(__file__))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--create',
        action='store_true',
        help=(
            "This demo shows how you can load parameter data "
            "from Excel MIRC into a new database using "
            "the functions in the data folder."
        )
    )
    parser.add_argument(
        '--pathways',
        action='store_true',
        help=(
            "This demo shows how you can use the ORM version "
            "of a scenario to run the RTR MIRC pathways."
        )
    )
    args = parser.parse_args()

    if args.create:
        create_db()

    if args.pathways:
        run_pathways()

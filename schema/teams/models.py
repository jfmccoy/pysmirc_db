import sqlalchemy as sa
from sqlalchemy.ext.declarative import declared_attr
from ..utils.base import Model


__all__ = [
    'Team', 'teams_users'
]


# Declarative tables

teams_users = sa.Table(
    'teams_users',
    Model.metadata,
    sa.Column('user_id', sa.Integer(), sa.ForeignKey('user.id')),
    sa.Column('team_id', sa.Integer(), sa.ForeignKey('team.id')),
    sa.UniqueConstraint('user_id', 'team_id')
)


# Model classes

class Team(Model):
    name = sa.Column(sa.String(255), nullable=False)

    owner_id = sa.Column(
        sa.Integer(),
        sa.ForeignKey('user.id'),
        nullable=False
    )
    owner = sa.orm.relationship(
        'User',
        backref=sa.orm.backref("managed_teams")
    )

    @declared_attr
    def members(cls):
        return sa.orm.relationship(
            'User', secondary=teams_users,
            enable_typechecks=False,
            collection_class=set,
            backref=sa.orm.backref(
                'teams', lazy='dynamic', enable_typechecks=False
            )
        )

    @property
    def all_members(self):
        # DO NOT Edit the result of this!
        # Changes will not be propagated back to the model
        return set(self.members).union([self.owner])

    def __repr__(self):
        mems = ", ".join(user.email for user in self.members)
        return (
            f"{self.__class__.__qualname__}('{self.name}'"
            f"{(': ' + mems) if mems else ''})"
        )

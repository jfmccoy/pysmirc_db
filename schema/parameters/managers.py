from .models import Parameter, ureg
from ..utils.serialize import register_serializer


__all__ = [
    'ParameterManager'
]


class NullParameter:
    value = None
    unit = None
    quantity = None
    source = None

    def __bool__(self):
        return False

    def __iter__(self):
        return self

    def __next__(self):
        raise StopIteration


@register_serializer(NullParameter)
def _ts_null_param(n: NullParameter):
    return None


class ParameterManager:
    """Mediates access to all variable parameters in a scenario.
    """

    _query_terms = {}

    def __init__(self, scenario, query_terms=None):
        self.scenario = scenario
        self._query_terms = {
            **(query_terms or {})
        }

    def __getattr__(self, name):
        return self.get_by_variable(name)

    def get_by_variable(self, variable):
        return self._get_param(variable=variable)

    def get_by_name(self, name):
        return self._get_param(name=name)

    def _get_param(self, **kwargs):
        s = self.scenario
        params = {}
        while s is not None:
            n_params = Parameter.query.filter_by(
                scenario_id=s.id,
                **kwargs,
                **{
                    k + '_id': v.id
                    for k, v in self._query_terms.items()
                }
            ).all()
            n_keys = []
            for p in n_params:
                n_keys.append(
                    f'{p.chemical_id},{p.media_id},'
                    f'{p.life_stage_id},{p.percentile_id}'
                    f'{p.food_id},{p.name}'
                )
            n_params = dict(zip(n_keys, n_params))
            params = {**n_params, **params}
            s = s.parent

        if not params:
            return NullParameter()

        params = list(params.values())
        if len(params) == 1:
            return params[0]
        else:
            return params

    def for_chemical(self, chemical):
        if isinstance(chemical, str):
            from ..builtins.models import Chemical
            chemical = Chemical.query.filter_by(
                name=chemical
            ).first()

        if chemical is None:
            return self
        q = {
            **self._query_terms,
            'chemical': chemical
        }
        return ParameterManager(self.scenario, q)

    @property
    def chemical(self):
        return self._query_terms.get('chemical')

    def for_media(self, media):
        if isinstance(media, str):
            from ..builtins.models import Product
            media = Product.query.filter_by(name=media).first()

        if media is None:
            return self
        q = {
            **self._query_terms,
            'media': media
        }
        return ParameterManager(self.scenario, q)

    @property
    def media(self):
        return self._query_terms.get('media')

    def at_life_stage(self, life_stage):
        if isinstance(life_stage, str):
            from ..builtins.models import LifeStage
            life_stage = LifeStage.query.filter_by(
                name=life_stage
            ).first()

        if life_stage is None:
            return self
        q = {
            **self._query_terms,
            'life_stage': life_stage
        }
        return ParameterManager(self.scenario, q)

    @property
    def life_stage(self):
        return self._query_terms.get('life_stage')

    def at_percentile(self, percentile):
        if isinstance(percentile, str):
            from ..builtins.models import Percentile
            percentile = Percentile.query.filter_by(
                name=percentile
            ).first()

        if percentile is None:
            return self
        q = {
            **self._query_terms,
            'percentile': percentile
        }
        return ParameterManager(self.scenario, q)

    @property
    def percentile(self):
        return self._query_terms.get('percentile')

    def for_food(self, food):
        if isinstance(food, str):
            from ..builtins.models import Product
            food = Product.query.filter_by(name=food).first()

        if food is None:
            return self
        q = {
            **self._query_terms,
            'food': food
        }
        return ParameterManager(self.scenario, q)

    @property
    def food(self):
        return self._query_terms.get('food')

    @property
    def unit_registry(self):
        return ureg

    def __repr__(self):
        return (
            f"{self.__class__.__qualname__}("
            f"{self.scenario.name}"
            f"{(', ' + self.chemical.cas_number) if self.chemical else ''}"
            f"{(', ' + self.media.name) if self.media else ''}"
            f"{(', ' + self.life_stage.name) if self.life_stage else ''}"
            f"{(', ' + self.percentile.name) if self.percentile else ''}"
            f"{(', ' + self.food.name) if self.food else ''}"
            ")"
        )


@register_serializer(ParameterManager)
def _ts_manager(pm: ParameterManager):
    return None

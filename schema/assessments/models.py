import sqlalchemy as sa
from ..parameters.models import as_quantity
from ..permissions.models import require_permissions
from ..utils.base import Model
from ..utils.mixins import TrackUpdatesMixin
from ..utils.serialize import register_serializer


__all__ = [
    'Assessment', 'Simulation', 'SimulationPercentile',
    'SimulationParameter', 'SimulationConsumptionBreakdown'
]


@require_permissions
class Assessment(Model, TrackUpdatesMixin):
    name = sa.Column(sa.String(120), nullable=False)
    description = sa.Column(sa.String(255))

    scenario_id = sa.Column(
        sa.Integer(), sa.ForeignKey('scenario.id'), nullable=False
    )
    scenario = sa.orm.relationship(
        'Scenario', backref=sa.orm.backref('assessments')
    )

    @property
    def safe_name(self):
        return self.name.replace(' ', '_')

    @property
    def short_description(self):
        if not self.description:
            return ''

        w = self.description.split()
        w.reverse()
        short = ''
        while len(short) < 75 and len(w):
            short += ' ' + w.pop()
        if len(w):
            short += ' ...'
        return short.strip()

    def __repr__(self):
        return f"{self.__class__.__qualname__}('{self.name}')"


@register_serializer(Assessment, queryable=True)
def _ts_assessment(assessment: Assessment):
    return {
        'id': assessment.id,
        'name': assessment.name,
        'description': assessment.description,
        'scenario': {
            'id': assessment.scenario.id,
            'name': assessment.scenario.name
        }
    }


class Simulation(Model, TrackUpdatesMixin):
    name = sa.Column(sa.String(120), nullable=False)

    assessment_id = sa.Column(
        sa.Integer(), sa.ForeignKey('assessment.id'), nullable=False
    )
    assessment = sa.orm.relationship(
        'Assessment',
        backref=sa.orm.backref(
            'simulations', cascade="all, delete-orphan", single_parent=True
        )
    )

    chemical_id = sa.Column(
        sa.Integer(), sa.ForeignKey('chemical.id'), nullable=False
    )
    chemical = sa.orm.relationship('Chemical')

    trim_scenario = sa.Column(sa.String(255), nullable=True)

    use_baf = sa.Column(sa.Boolean(), nullable=False, default=False)

    def get_parameter(self, variable=None, name=None):
        for p in self.parameters:
            if variable is not None and p.variable == variable:
                return p
            elif name is not None and p.name == name:
                return p
        return None

    def __repr__(self):
        return (
            f"{self.__class__.__qualname__}("
            f"'{self.assessment.name}', '{self.name}'"
            ")"
        )


class SimulationPercentile(Model):
    simulation_id = sa.Column(
        sa.Integer(), sa.ForeignKey('simulation.id'), nullable=False
    )
    simulation = sa.orm.relationship(
        'Simulation',
        backref=sa.orm.backref(
            'percentiles', cascade="all, delete-orphan", single_parent=True
        )
    )

    food_id = sa.Column(
        sa.Integer(), sa.ForeignKey('product.id'), nullable=True
    )
    food = sa.orm.relationship('Product')

    percentile_id = sa.Column(
        sa.Integer(), sa.ForeignKey('percentile.id'), nullable=False
    )
    percentile = sa.orm.relationship('Percentile')

    def __repr__(self):
        return (
            f"{self.__class__.__qualname__}("
            f"'{self.simulation.name}', "
            f"{(self.food.name + ', ') if self.food else 'body weight, '}"
            f"{self.percentile}"
            ")"
        )


class SimulationParameter(Model):
    simulation_id = sa.Column(
        sa.Integer(), sa.ForeignKey('simulation.id'), nullable=False
    )
    simulation = sa.orm.relationship(
        'Simulation',
        backref=sa.orm.backref(
            'parameters', cascade="all, delete-orphan", single_parent=True
        )
    )

    variable = sa.Column(sa.String(60), nullable=False)
    name = sa.Column(sa.String(255), nullable=True)

    value = sa.Column(sa.Float(), nullable=False)
    unit = sa.Column(sa.String(36), nullable=True)

    source = sa.Column(sa.String(255), nullable=True)

    @property
    def quantity(self):
        return as_quantity(self.value, self.unit)

    def __repr__(self):
        return (
            f"{self.__class__.__qualname__}("
            f"'{self.simulation.name}', {self.variable}, {self.quantity}"
            ")"
        )


class SimulationConsumptionBreakdown(Model):
    simulation_id = sa.Column(
        sa.Integer(), sa.ForeignKey('simulation.id'), nullable=False
    )
    simulation = sa.orm.relationship(
        'Simulation',
        backref=sa.orm.backref(
            'consumption_breakdowns', cascade="all, delete-orphan",
            single_parent=True
        )
    )

    subfood_id = sa.Column(
        sa.Integer(), sa.ForeignKey('product.id'), nullable=False
    )
    subfood = sa.orm.relationship('Product')

    fraction = sa.Column(sa.Float(), nullable=False)

    source = sa.Column(sa.String(255), nullable=True)

    def __repr__(self):
        return (
            f"{self.__class__.__qualname__}("
            f"'{self.simulation.name}', {self.subfood.name}, {self.fraction}"
            ")"
        )

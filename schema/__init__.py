# These imports are necessary for running alembic
# with the --autogenerate flag:

# 0. Import serialization utils
from .utils.serialize import *  # noqa

# 1. Import the base Model class
from .utils.base import Model  # noqa

# 2. Import all model files
from .users.models import *  # noqa
from .teams.models import *  # noqa
from .permissions.models import *  # noqa
from .builtins.models import *  # noqa
from .scenarios.models import *  # noqa
from .parameters.models import *  # noqa
from .parameters.models import ureg as MircUnitRegistry  # noqa
from .parameters.managers import *  # noqa
from .assessments.models import *  # noqa

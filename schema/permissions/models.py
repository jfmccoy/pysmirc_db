import sqlalchemy as sa
from enum import Enum
from sqlalchemy.ext.declarative import declared_attr
from ..utils.base import Model


__all__ = [
    'Permission', 'AccessModel', 'UserPermissions', 'TeamPermissions'
]


# Model classes

class DefaultPermissions(Enum):
    manage = 1
    edit = 2
    view = 3


class Permission(Model):
    id = sa.Column(sa.Integer(), primary_key=True)
    name = sa.Column(sa.String(40), unique=True)

    subpermission_id = sa.Column(
        sa.Integer(),
        sa.ForeignKey('permission.id'),
        nullable=True
    )
    _subpermission = sa.orm.relationship(
        'Permission',
        remote_side=[id]
    )

    def entails(self, permission):
        if isinstance(permission, str):
            permission = Permission.query.filter_by(name=permission).first()

        if (permission == self or (
            self._subpermission is not None and (
                permission == self._subpermission or
                self._subpermission.entails(permission)
            )
        )):
            return True

        return False

    def __repr__(self):
        return f"{self.__class__.__qualname__}('{self.name}')"


class AccessModel(Model):
    def _get_permission(self, name):
        return Permission.query.filter_by(name=name).first()

    def grant_user(self, user, permission):
        if isinstance(permission, str):
            permission = self._get_permission(permission)

        if not permission:
            raise TypeError

        self.user_permissions.add(
            UserPermissions(user=user, permission=permission)
        )

    def grant_team(self, team, permission):
        if isinstance(permission, str):
            permission = self._get_permission(permission)

        if not permission:
            raise TypeError

        self.team_permissions.add(
            TeamPermissions(team=team, permission=permission)
        )

    def grant(self, entity, permission):
        entity_table = entity.__tablename__

        if entity_table == 'team':
            self.grant_team(entity, permission)
        elif entity_table == 'user':
            self.grant_user(entity, permission)
        else:
            raise TypeError

    def revoke_user(self, user, permission):
        if isinstance(permission, str):
            permission = self._get_permission(permission)

        if not permission:
            raise TypeError

        perms = [
            p for p in self.user_permissions
            if p.user == user and p.permission == permission
        ]

        for p in perms:
            self.user_permissions.remove(p)

    def revoke_team(self, team, permission):
        if isinstance(permission, str):
            permission = self._get_permission(permission)

        if not permission:
            raise TypeError

        perms = [
            p for p in self.team_permissions
            if p.team == team and p.permission == permission
        ]

        for p in perms:
            self.team_permissions.remove(p)

    def revoke(self, entity, permission):
        entity_table = entity.__tablename__

        if entity_table == 'team':
            self.revoke_team(entity, permission)
        elif entity_table == 'user':
            self.revoke_user(entity, permission)
        else:
            raise TypeError

    def allows_user(self, user, permission):
        if isinstance(permission, str):
            permission = self._get_permission(permission)

        if not permission:
            raise TypeError

        if user.has_role('superuser'):
            return True

        perms = [
            p for p in self.user_permissions
            if (p.user == user or p.user is None) and
            (
                p.permission == permission or
                p.permission.entails(permission)
            )
        ]
        if perms:
            return True

        for team in set(user.teams).union(user.managed_teams):
            if self.allows_team(team, permission):
                return True

        return False

    def allows_team(self, team, permission):
        if isinstance(permission, str):
            permission = self._get_permission(permission)

        if not permission:
            raise TypeError

        perms = [
            p for p in self.team_permissions
            if (p.team == team or p.team is None) and
            (
                p.permission == permission or
                p.permission.entails(permission)
            )
        ]
        if perms:
            return True

        return False

    def allows(self, entity, permission):
        entity_table = entity.__tablename__

        if entity_table == 'team':
            return self.allows_team(entity, permission)
        elif entity_table == 'user':
            return self.allows_user(entity, permission)
        else:
            raise TypeError

    def __repr__(self):
        return f"{self.__class__.__qualname__}('{self.id}')"


class AccessPermissionsMixin:
    @declared_attr
    def access_model_id(cls):
        return sa.Column(
            sa.Integer(), sa.ForeignKey('access_model.id'), nullable=False
        )

    @declared_attr
    def access_model(cls):
        return sa.orm.relationship(
            'AccessModel', backref=sa.orm.backref(
                cls.__tablename__, collection_class=set,
                cascade="all, delete-orphan", single_parent=True
            )
        )

    @declared_attr
    def permission_id(cls):
        return sa.Column(
            sa.Integer(), sa.ForeignKey('permission.id'), nullable=False
        )

    @declared_attr
    def permission(cls):
        return sa.orm.relationship('Permission')


class UserPermissions(AccessPermissionsMixin, Model):
    # If user is null, this is treated as global
    user_id = sa.Column(
        sa.Integer(), sa.ForeignKey('user.id'), nullable=True
    )
    user = sa.orm.relationship(
        'User', backref=sa.orm.backref(
            "permissions", collection_class=set,
            cascade="all, delete-orphan", single_parent=True
        )
    )

    __table_args__ = (
        sa.UniqueConstraint(
            'access_model_id', 'user_id', 'permission_id'
        ),
    )

    def __repr__(self):
        return (
            f"{self.__class__.__qualname__}("
            f"{self.id}, {self.user}, {self.permission}"
            ")"
        )


class TeamPermissions(AccessPermissionsMixin, Model):
    # If team is null, this is treated as global
    team_id = sa.Column(
        sa.Integer(), sa.ForeignKey('team.id'), nullable=True
    )
    team = sa.orm.relationship(
        'Team', backref=sa.orm.backref(
            "permissions", collection_class=set,
            cascade="all, delete-orphan", single_parent=True
        )
    )

    __table_args__ = (
        sa.UniqueConstraint(
            'access_model_id', 'team_id', 'permission_id'
        ),
    )

    def __repr__(self):
        return (
            f"{self.__class__.__qualname__}("
            f"{self.id}, {self.team}, {self.permission}"
            ")"
        )


# Helper functions
def require_permissions(cls):
    cls.access_model_id = sa.Column(
        sa.Integer(),
        sa.ForeignKey('access_model.id'),
        nullable=False
    )

    cls.access_model = sa.orm.relationship(
        'AccessModel',
        backref=sa.orm.backref(
            f"{cls.__tablename__}s", uselist=False
        )
    )

    @sa.event.listens_for(cls, 'before_insert')
    def add_access_model(mapper, connection, target):
        tbl = AccessModel.__table__
        r = connection.execute(
            tbl.insert()
        )
        target.access_model_id = r.inserted_primary_key[0]

    @sa.event.listens_for(cls, 'after_delete')
    def rem_access_model(mapper, connection, target):
        tbl = AccessModel.__table__
        connection.execute(
            tbl.delete().where(tbl.c.id == target.access_model_id)
        )

    return cls

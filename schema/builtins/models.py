import sqlalchemy as sa
from ..parameters.managers import ParameterManager
from ..utils.base import Model
from ..utils.serialize import register_serializer


__all__ = [
    'Chemical', 'Product', 'LifeStage', 'Percentile'
]


class Chemical(Model):
    """Represents the 'fixed' properties of a chemical.
    """
    cas_number = sa.Column(sa.String(255), nullable=False)
    hap_number = sa.Column(sa.String(255))
    hap_name = sa.Column(sa.String(255))
    inorganic = sa.Column(sa.Boolean(), nullable=False)
    mutagenic = sa.Column(sa.Boolean(), nullable=False)
    epa_evidence_weight = sa.Column(sa.String(255))

    __table_args__ = (
        sa.UniqueConstraint(
            'cas_number', 'hap_name'
        ),
    )

    # Just for convenience
    @property
    def name(self):
        return self.hap_name

    @property
    def organic(self):
        return not self.inorganic

    @property
    def epa_woe(self):
        return self.epa_evidence_weight

    def get_parameters(self, scenario):
        return ParameterManager(scenario).for_chemical(self)

    def __repr__(self):
        return (
            f"{self.__class__.__qualname__}("
            f"'{self.cas_number}', "
            f"'{self.hap_name}'"
            ")"
        )


@register_serializer(Chemical)
def _ts_chemical(c: Chemical):
    serial = {
        'id': c.id,
        'cas_number': c.cas_number,
        'hap_number': c.hap_number,
        'hap_name': c.hap_name,
        'inorganic': c.inorganic,
        'mutagenic': c.mutagenic,
        'EPA_WOE': c.epa_woe
    }
    return serial


class NamedModelMixin:
    name = sa.Column(sa.String(255), unique=True, nullable=False)

    def __repr__(self):
        return f"{self.__class__.__qualname__}('{self.name}')"


class Product(NamedModelMixin, Model):
    """Represents a specific product which is ingested
    by human or animal consumers.
    """
    category_id = sa.Column(
        sa.Integer(),
        sa.ForeignKey('product.id'),
        nullable=True
    )
    category = sa.orm.relationship(
        'Product', remote_side='Product.id',
        backref=sa.orm.backref("subcategories", cascade='all, delete-orphan')
    )

    is_food = sa.Column(sa.Boolean(), nullable=False, default=False)
    is_feed = sa.Column(sa.Boolean(), nullable=False, default=False)

    def is_a(self, name, strict=False):
        if self.name == name:
            return True

        if not strict and self.category:
            return self.category.is_a(name)

        return False

    def get_parameters(self, scenario):
        return ParameterManager(scenario).for_product(self)


@register_serializer(Product)
def _ts_product(p: Product):
    serial = {
        'id': p.id,
        'name': p.name
    }
    if p.category:
        serial['category'] = p.category.name
    subcategories = [sub.name for sub in p.subcategories]
    if subcategories:
        serial['subcategories'] = subcategories
    return serial


class LifeStage(NamedModelMixin, Model):
    """Represents an age group (i.e., "Adult").
    """
    duration = sa.Column(sa.Float())
    duration_unit = sa.Column(sa.String(255))


@register_serializer(LifeStage)
def _ts_life_stage(ls: LifeStage):
    return {
        'id': ls.id,
        'name': ls.name,
        'duration': ls.duration,
        'duration_unit': ls.duration_unit
    }


class Percentile(NamedModelMixin, Model):
    """Represents a targeted portion of a study population.
    """
    pass


@register_serializer(Percentile)
def _ts_percentile(pct: Percentile):
    return {
        'id': pct.id,
        'name': pct.name
    }

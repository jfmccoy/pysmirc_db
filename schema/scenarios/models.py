import sqlalchemy as sa
from ..parameters.managers import ParameterManager
from ..permissions.models import require_permissions
from ..utils.base import Model
from ..utils.serialize import register_serializer


__all__ = [
    'Scenario'
]


@require_permissions
class Scenario(Model):
    """Represents an ingestion scenario, and provides access
    to all parameters for performing MIRC calculations.
    """
    id = sa.Column(sa.Integer(), primary_key=True)
    name = sa.Column(sa.String(255), unique=True, nullable=False)
    is_builtin = sa.Column(sa.Boolean(), nullable=False, default=False)
    notes = sa.Column(sa.String(255))

    parent_id = sa.Column(
        sa.Integer(),
        sa.ForeignKey('scenario.id'),
        nullable=True
    )
    parent = sa.orm.relationship(
        'Scenario',
        remote_side=[id],
        backref=sa.orm.backref("child_scenarios")
    )

    @property
    def parameters(self):
        return ParameterManager(self)

    def __repr__(self):
        return f"{self.__class__.__qualname__}('{self.name}')"


@register_serializer(Scenario, queryable=True, auto_recursive=False)
def _ts_scenario(scenario: Scenario):
    p = None
    if scenario.parent:
        p = {
            'id': scenario.parent.id,
            'name': scenario.parent.name
        }
    return {
        'id': scenario.id,
        'name': scenario.name,
        'parent': p,
        'notes': scenario.notes
    }

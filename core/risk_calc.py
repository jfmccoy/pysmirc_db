from .equations import *
from .utils import printvals
from schema import LifeStage


__all__ = ['assess_risk']


def assess_risk(
    scenario, product, chemical,
    concentration=None, concentration_fat=None, concentration_aq=None,
    ingestion_percentile='Pmean', body_weight_percentile='Pmean'
):
    if (
        concentration is None and
        concentration_fat is None and
        concentration_aq is None
    ):
        return None

    chem_params = scenario.parameters.for_chemical(chemical)
    product_params = scenario.parameters.for_media(product)

    loss_factors = product_params.LF
    try:
        iter(loss_factors)
    except TypeError:
        loss_factors = [loss_factors]

    print(
        "In this scenario, loss factors "
        f"for {product} = {loss_factors}"
    )

    ureg = scenario.parameters.unit_registry  # Get active unit registry

    FC = product_params.FC.value or 1
    EF = product_params.EF.quantity or (350 * ureg('day'))

    print(f'\nGot parameters for {product} from scenario:')
    printvals(FC=FC, EF=EF)

    adj_factor = None
    if product.is_a('fish'):
        # Get params by-chemical by-animal for product
        chem_animal_params = chem_params.for_media(product)
        adj_factor = chem_animal_params.FishAdjFactor.value
    elif product.name == 'soil':
        adj_factor = chem_params.SoilAdjFactor.value

    adj_factor = adj_factor or 1
    print(
        "\nGot a cooking adjustment factor of "
        f"{adj_factor} for {product} for {chemical} in scenario"
    )

    pct_params = scenario.parameters.for_food(
        product
    ).at_percentile(ingestion_percentile)

    base_intake = {}
    adafs = {}

    irs = pct_params.IR
    try:
        iter(irs)
    except TypeError:
        irs = [irs]

    for ir_param in irs:
        if ir_param.media is not None:
            continue

        age = ir_param.life_stage

        IR = ir_param.quantity
        if IR is None:
            if product.name == 'water':
                IR = 0 * ureg('mL/day')
            else:
                IR = 0 * ureg('g/day/kg')

        print(f"\nGot IR = {IR} for {product} for {age} in scenario")

        if product.name == 'breast milk':
            f_mbm = product_params.f_mbm.value
            AE_inf = chem_params.for_media(product).AE_inf.value
            BW_inf = scenario.parameters.at_percentile(
                body_weight_percentile
            ).at_life_stage(age).BW.quantity
            print(f"Got BW_inf = {BW_inf} for {age} in scenario")

            # We need to round this to make it match our old results,
            # which always used a rounded infant bw instead of the
            # unrounded version stored in the main bw table
            BW_inf = round(BW_inf, 1)
            print(f"Rounded BW_inf = {BW_inf}")

            intake = get_average_daily_dose_to_nursing_infant(
                concentration_fat, f_mbm, concentration_aq,
                IR, AE_inf, EF, BW_inf
            )
        else:
            # Check if the ingestion rate needs to be body-weight adjusted
            if not IR.check('[mass]/[time]/[mass]'):
                body_weight = scenario.parameters.at_percentile(
                    body_weight_percentile
                ).at_life_stage(age).BW.quantity
                print(f"Got body_weight = {body_weight} for {age} in scenario")

                IR = IR / body_weight
                print(f"Adjusted IR = {IR}")

            intake = concentration * IR
            intake = intake * adj_factor
            intake = intake * FC * (EF / (365 * ureg('day')))

            for lf in loss_factors:
                intake = intake * (1 - lf.value)

        intake = intake.to('mg/day/kg')

        print(
            f"Calculated {chemical} intake from {product} "
            f"for {age}: {intake}"
        )
        base_intake[age.name] = intake

        adaf = chem_params.at_life_stage(age).ADAF.quantity
        adafs[age.name] = adaf

    ages = {
        ls.name: ls.duration * ureg(ls.duration_unit)
        for ls in LifeStage.query.all()
        if ls.name != 'Pregnant Mother'
    }
    lifespan = max(70 * ureg('year'), sum(ages.values()))

    RfD = chem_params.RfD.quantity
    print(f"\nGot RfD = {RfD} for {chemical} in scenario")

    CSF = chem_params.CSF.quantity
    print(f"\nGot CSF = {CSF} for {chemical} in scenario")

    risk_data = {}

    LADD = 0
    LADD_adj = None
    for age, span in ages.items():
        val = base_intake.get(age, 0 * ureg('mg/day/kg'))
        LADD += val * span / lifespan

        adj = None
        adaf = adafs.get(age)
        if adaf:
            print(
                "\nIn this scenario, mutagenic adjustment factor "
                f"for {chemical} at {age} = {adaf}"
            )
            adj = val * adaf
            print(
                f"Calculated adjusted {chemical} intake from {product} "
                f"for {age}: {adj}"
            )
            if LADD_adj is None:
                LADD_adj = 0
            LADD_adj += adj * span / lifespan

        hq = None
        if RfD:
            i = adj if adj is not None else val
            hq = i / RfD

        elcr = None
        if CSF:
            i = adj if adj is not None else val
            elcr = i * CSF

        risk_data[age] = {
            'intake': val,
            'adjusted_intake': adj,
            'hazard_quotient': hq,
            'risk_factor': elcr
        }

    print(
        f"\nCalculated {chemical} intake from {product} "
        f"for LADD: {LADD}"
    )

    if LADD_adj:
        print(
            f"\nCalculated adjusted {chemical} intake from {product} "
            f"for LADD: {LADD_adj}"
        )

    LADD_hq = None
    if RfD:
        i = LADD_adj if LADD_adj is not None else LADD
        LADD_hq = i / RfD

    LADD_elcr = None
    if CSF:
        i = LADD_adj if LADD_adj is not None else LADD
        LADD_elcr = i * CSF

    risk_data['Lifetime'] = {
        'intake': LADD,
        'adjusted_intake': LADD_adj,
        'hazard_quotient': LADD_hq,
        'risk_factor': LADD_elcr
    }

    return risk_data

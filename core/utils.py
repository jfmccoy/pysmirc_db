import inspect
import logging
import os
from functools import wraps
from pprint import pformat


def printvals(logger=None, **vals):
    """A helper that simply prints a list of key/value pairs"""

    if logger is not None:
        pr = logger.info
    else:
        pr = print

    for k, v in vals.items():
        pr(f'\t{k} = {pformat(v)}')


def input_units(**arg_checks):
    def decorated(f):

        param_names = list(inspect.signature(f).parameters.keys())

        @wraps(f)
        def validated(*args, **kwargs):
            # Get all inputs, and convert args to kwargs based on signature
            vals = {**kwargs}
            for i, arg in enumerate(args):
                vals[param_names[i]] = arg
            # Check all inputs in arg_checks
            for k, v in vals.items():
                if k in arg_checks:
                    if isinstance(v, list):
                        for subv in v:
                            check_units(k, subv, dimensionality=arg_checks[k])
                    else:
                        check_units(k, v, dimensionality=arg_checks[k])
            # If we succeeded, call the function
            return f(*args, **kwargs)
        return validated
    return decorated


def check_units(name, val, *, dimensionality=None):
    if val is None:
        return  # This is ok

    if not dimensionality:
        if hasattr(val, 'dimensionality') and not val.check(''):
            raise TypeError(
                f'{name} must be unitless, but the value passed was {val}'
            )
    else:
        real_dimension = (
            len(dimensionality.split('/')) == 1 or  # i.e., not a relation
            len(set(dimensionality.split('/'))) > 1  # e.g., not mass/mass
        )

        if not real_dimension and not hasattr(val, 'dimensionality'):
            return  # this is ok

        if not hasattr(val, 'dimensionality') or not val.check(dimensionality):
            raise TypeError(
                f'{name} must be a quantity of {dimensionality},'
                f' but the value passed was {val}'
            )


def output_units(*units):
    def decorated(f):
        @wraps(f)
        def transformed(*args, **kwargs):
            result = f(*args, **kwargs)

            if isinstance(result, tuple):
                converted = []
                for i, r in enumerate(result):
                    if hasattr(r, 'dimensionality'):
                        converted.append(r.to(units[i]))
                    else:
                        converted.append(r)
                return tuple(converted)

            elif hasattr(result, 'dimensionality'):
                return result.to(units[0])

            return result
        return transformed
    return decorated


def make_logger(name):
    logger = logging.Logger(name)
    h = logging.StreamHandler()

    env = os.getenv('FLASK_ENV', 'production')
    if env == 'production':
        h.setLevel(logging.INFO)
    else:
        h.setLevel(logging.DEBUG)

    f = logging.Formatter(
        '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
    )
    h.setFormatter(f)

    logger.addHandler(h)

    return logger

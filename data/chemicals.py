

def insert_chem_definitions(df, db):
    from schema import Chemical

    found_new = False

    for _, row in df.iterrows():
        chem = Chemical.query.filter_by(
            cas_number=row['CAS'],
            hap_name=row['HAP_Name']
        ).first()
        if not chem:
            found_new = True
            is_mut = row['Mutagenic'] == -1
            chem = Chemical(
                cas_number=row['CAS'],
                hap_name=row['HAP_Name'],
                hap_number=row['HAP_NO'],
                inorganic=row['Inorganic'],
                mutagenic=is_mut,
                epa_evidence_weight=row['EPA_WOE']
            )
            print(f'Inserting {chem} ...')
            db.session.add(chem)

    db.session.commit()
    return found_new

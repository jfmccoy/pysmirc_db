
DEFAULT_PRODUCTS = [
    {'name': 'water', 'food': True},
    {'name': 'soil', 'food': True},

    {'name': 'plant'},
    {'name': 'protected plant', 'category': 'plant'},
    {
        'name': 'protected vegetable',
        'category': 'protected plant',
        'food': True
    },
    {'name': 'protected fruit', 'category': 'protected plant', 'food': True},
    {'name': 'root', 'category': 'protected plant', 'food': True},
    {'name': 'grain', 'category': 'protected plant', 'feed': True},

    {'name': 'exposed plant', 'category': 'plant'},
    {'name': 'exposed vegetable', 'category': 'exposed plant', 'food': True},
    {'name': 'exposed fruit', 'category': 'exposed plant', 'food': True},
    {'name': 'forage', 'category': 'exposed plant', 'feed': True},
    {'name': 'silage', 'category': 'exposed plant', 'feed': True},

    {'name': 'animal'},
    {'name': 'livestock', 'category': 'animal'},
    {'name': 'beef', 'category': 'livestock', 'food': True},
    {'name': 'dairy', 'category': 'beef', 'food': True},
    {'name': 'pork', 'category': 'livestock', 'food': True},
    {'name': 'poultry', 'category': 'livestock', 'food': True},
    {'name': 'eggs', 'category': 'poultry', 'food': True},

    {'name': 'fish', 'category': 'animal', 'food': True},
    {'name': 'macrophyte', 'category': 'fish'},
    {'name': 'zooplankton', 'category': 'fish'},
    {'name': 'water column herbivore', 'category': 'fish'},
    {'name': 'water column omnivore', 'category': 'fish'},
    {'name': 'water column carnivore', 'category': 'fish'},
    {'name': 'benthic invertebrate', 'category': 'fish'},
    {'name': 'benthic omnivore', 'category': 'fish'},
    {'name': 'benthic carnivore', 'category': 'fish'},

    {'name': 'breast milk', 'food': True}
]

DEFAULT_PERCENTILES = [
    'P05', 'P10', 'P50',
    'P90', 'P95', 'P99',
    'Pmean',
    'None'
]

DEFAULT_AGE_GROUPS = [
    {'name': 'Child <1', 'duration': '1', 'duration_unit': 'year'},
    {'name': 'Child 1-2', 'duration': '2', 'duration_unit': 'year'},
    {'name': 'Child 3-5', 'duration': '3', 'duration_unit': 'year'},
    {'name': 'Child 6-11', 'duration': '6', 'duration_unit': 'year'},
    {'name': 'Child 12-19', 'duration': '8', 'duration_unit': 'year'},
    {'name': 'Adult', 'duration': '50', 'duration_unit': 'year'},

    # For maternal body weight
    {'name': 'Pregnant Mother', 'duration': '274', 'duration_unit': 'day'}
]


def insert_default_builtins(db):
    insert_builtin_products(db)
    insert_builtin_percentiles(db)
    insert_builtin_age_groups(db)


def insert_builtin_products(db):
    from schema import Product

    for prod in DEFAULT_PRODUCTS:
        cat = prod.get('category')
        c = None
        if cat:
            c = Product.query.filter_by(name=cat).first()
            if not c:
                c = Product(name=cat)
                db.session.add(c)

        nm = prod['name'].lower()
        p = Product.query.filter_by(name=nm).first()
        if not p:
            p = Product(name=nm, category=c)
            db.session.add(p)
        p.is_food = prod.get('food', False)
        p.is_feed = prod.get('feed', False)

    db.session.commit()


def insert_builtin_percentiles(db):
    from schema import Percentile

    for pct in DEFAULT_PERCENTILES:
        p = Percentile.query.filter_by(name=pct).first()
        if not p:
            p = Percentile(name=pct)
            db.session.add(p)

    db.session.commit()


def insert_builtin_age_groups(db):
    from schema import LifeStage

    for ls in DEFAULT_AGE_GROUPS:
        age = ls['name']
        ag = LifeStage.query.filter_by(name=age).first()
        if not ag:
            ag = LifeStage(
                name=age,
                duration=ls.get('duration'),
                duration_unit=ls.get('duration_unit')
            )
            db.session.add(ag)

    db.session.commit()


DEFAULT_SCENARIOS = [
    {
        'name': 'HHRAP/EFH11',
        'parent': None
    },
    {
        'name': 'RTR Screening',
        'parent': 'HHRAP/EFH11'
    }
]


def insert_default_scenarios(db):
    from schema import Scenario, Permission
    from schema.permissions.models import DefaultPermissions

    for p in DefaultPermissions:
        perm = Permission.query.filter_by(name=p.name).first()
        if not perm:
            perm = Permission(
                name=p.name
            )
            db.session.add(perm)
    db.session.commit()
    for perm in Permission.query.all():
        if perm.name == 'manage':
            perm.subpermission_id = 2  # edit
        elif perm.name == 'edit':
            perm.subpermission_id = 3  # view

    p_view = Permission.query.filter_by(name='view').first()

    for scenario in DEFAULT_SCENARIOS:
        parent = None
        if scenario['parent']:
            parent = Scenario.query.filter_by(
                name=scenario['parent']).first()

        s = Scenario.query.filter_by(name=scenario['name']).first()
        if not s:
            s = Scenario(
                name=scenario['name'],
                parent=parent,
                is_builtin=True
            )
            db.session.add(s)
            db.session.commit()  # Commit now to create access model
            s.access_model.grant_user(None, p_view)  # Global view access

    db.session.commit()

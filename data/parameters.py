import numpy as np
import pandas as pd


def get_scenario(name='HHRAP/EFH11'):
    from schema import Scenario
    return Scenario.query.filter_by(name=name).first()


def load_default_params(filestream, db):
    import os
    import sys
    sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)))
    try:
        return load_excel_params(filestream, db)
    except Exception as e:
        import traceback
        traceback.print_exc()
        return [f'Unable to read parameters: {e}']


def load_excel_params(filestream, db):
    dfs = pd.read_excel(filestream, sheet_name=None)

    new_vals = []

    chem_df = dfs.get('Lookups-Chemical', None)
    if chem_df is not None:
        new_chem = load_chem_params(chem_df, db)
        new_vals.append(new_chem)

    baf_df = dfs.get('BAFs', None)
    if baf_df is not None:
        new_baf = load_baf_params(baf_df, db)
        new_vals.append(new_baf)

    eq_df = dfs.get('Equations', None)
    if eq_df is not None:
        new_bm = load_breast_milk_params(eq_df, db)
        new_vals.append(new_bm)

    exp_df = dfs.get('Lookups-Exposure', None)
    if exp_df is not None:
        new_exp = load_exposure_params(exp_df, db)
        new_vals.append(new_exp)

    found_new = [
        msg for msgs in new_vals for msg in msgs
    ]

    return found_new


def rows_to_headers(df, rows=[0]):
    head_arrs = [
        df.iloc[i].fillna(method='ffill')
        for i in rows
    ]
    head_count = len(head_arrs)

    ret = df[head_count:]
    if head_count == 1:
        ret.columns = head_arrs[0]
    else:
        ret.columns = pd.MultiIndex.from_arrays(head_arrs)

    return ret.reset_index(drop=True).sort_index()


def load_chem_params(df, db):
    from .chemicals import insert_chem_definitions
    df.replace('-', np.nan, inplace=True)
    df = df.iloc[6:].reset_index(drop=True)

    found_new = []

    chem_df = df.iloc[:, 1:]
    chem_df = rows_to_headers(chem_df, rows=[0, 1])
    chem_df = chem_df.where((pd.notnull(chem_df)), None)

    definitions = pd.concat(
        [
            chem_df['General'][[
                'CAS', 'HAP_Name', 'HAP_NO'
            ]],
            chem_df['General'][[
                'Inorganic'
            ]].fillna(False),
            chem_df['Toxicity'][['EPA_WOE']],
            chem_df['Mutagenicity / ADAF Factors'][['Mutagenic']]
        ], axis=1
    )
    if insert_chem_definitions(definitions, db):
        found_new.append('Added new Chemical(s)')

    chem_params = pd.concat(
        [
            chem_df['General'][[
                'CAS', 'HAP_Name', 'Fw', 'Kds'
            ]],
            chem_df['Toxicity'][[
                'RfD', 'Units_RfD', 'NC_Source',
                'CSF', 'Units_CSF', 'C_Source'
            ]],
            chem_df['Soil'][[
                'SoilAdjFactor'
            ]],
        ], axis=1
    )
    if insert_chem_params(chem_params, db):
        found_new.append('Added new Chemical Parameter(s)')

    bm_params = pd.concat(
        [
            chem_df['General'][[
                'CAS', 'HAP_Name'
            ]],
            chem_df['Breast Milk'],
        ], axis=1
    )
    if insert_breast_milk_chem_params(bm_params, db):
        found_new.append('Added new Breast Milk Parameter(s)')

    adaf_params = pd.concat(
        [
            chem_df['General'][[
                'CAS', 'HAP_Name'
            ]],
            chem_df['Mutagenicity / ADAF Factors'],
        ], axis=1
    )
    if insert_mutagenic_adjustment_params(adaf_params, db):
        found_new.append('Added new ADAF Parameter(s)')

    plant_params = pd.concat(
        [
            chem_df['General'][[
                'CAS', 'HAP_Name'
            ]],
            chem_df['Plant Products- HAP Specific'],
            chem_df['Exp Fruit'],
            chem_df['Exp Veg'],
            chem_df['Forage'],
            chem_df['Grain'],
            chem_df['Prot Fruit'],
            chem_df['Prot Veg'],
            chem_df['Root'],
            chem_df['Silage']
        ], axis=1
    )
    if insert_plant_chem_params(plant_params, db):
        found_new.append('Added new Plant-Chemical Parameter(s)')

    animal_params = pd.concat(
        [
            chem_df['General'][[
                'CAS', 'HAP_Name'
            ]],
            chem_df['Animal Products'],
            chem_df['Fish'][[
                'FishCookingAdjFactor'
            ]],
        ], axis=1
    )
    if insert_animal_chem_params(animal_params, db):
        found_new.append('Added new Animal-Chemical Parameter(s)')

    return found_new


def insert_chem_params(df, db):
    from schema import Chemical
    from schema import Parameter
    found_new = False

    s = get_scenario()

    for _, row in df.iterrows():
        chem = Chemical.query.filter_by(
            cas_number=row['CAS'],
            hap_name=row['HAP_Name']
        ).first()
        if not chem:
            print(
                f"Chemical('{row['CAS']}', {row['HAP_Name']}')",
                f"not found for Chemical Parameters"
            )
            continue
        params = Parameter.query.filter_by(
            scenario_id=s.id,
            chemical_id=chem.id
        )

        if row['Fw'] is not None:
            Fw = params.filter_by(variable='Fw').first()
            if not Fw:
                Fw = Parameter(
                    scenario=s,
                    chemical=chem,
                    name='fraction wet deposition',
                    variable='Fw',
                    value=row['Fw']
                )
                found_new = True
                print(f'Inserting {Fw} ...')
                db.session.add(Fw)

        if row['Kds'] is not None:
            Kds = params.filter_by(variable='Kds').first()
            if not Kds:
                Kds = Parameter(
                    scenario=s,
                    chemical=chem,
                    name='soil-water partition coefficient',
                    variable='Kds',
                    value=row['Kds'],
                    unit='L/kg'
                )
                found_new = True
                print(f'Inserting {Kds} ...')
                db.session.add(Kds)

        if row['SoilAdjFactor'] is not None:
            soil_adj = params.filter_by(variable='SoilAdjFactor').first()
            if not soil_adj:
                soil_adj = Parameter(
                    scenario=s,
                    chemical=chem,
                    name='soil bioavailability factor',
                    variable='SoilAdjFactor',
                    value=row['SoilAdjFactor']
                )
                found_new = True
                print(f'Inserting {soil_adj} ...')
                db.session.add(soil_adj)

        if row['CSF'] is not None:
            CSF = params.filter_by(variable='CSF').first()
            if not CSF:
                CSF = Parameter(
                    scenario=s,
                    chemical=chem,
                    name='cancer slope factor',
                    variable='CSF',
                    value=row['CSF'],
                    unit=str(row[
                        'Units_CSF'
                    ]).replace('1/(mg/kg/d)', '(mg/kg/day)^-1'),
                    source=row['C_Source']
                )
                if CSF.unit == 'CAL':
                    CSF.notes = (
                        'Units were listed as "CAL", which is the source;'
                        ' changed units to the default value'
                    )
                    CSF.source = 'CAL'
                    CSF.unit = '(mg/kg/day)^-1'
                elif CSF.unit == 'None':
                    CSF.notes = (
                        'CSF was listed without any units;'
                        ' set units to the default value'
                    )
                    CSF.unit = '(mg/kg/day)^-1'
                found_new = True
                print(f'Inserting {CSF} ...')
                db.session.add(CSF)

        if row['RfD'] is not None:
            RfD = params.filter_by(variable='RfD').first()
            if not RfD:
                RfD = Parameter(
                    scenario=s,
                    chemical=chem,
                    name='reference dose',
                    variable='RfD',
                    value=row['RfD'],
                    unit=str(row[
                        'Units_RfD'
                    ]).replace('mg/kg/d', 'mg/kg/day'),
                    source=row['NC_Source']
                )
                found_new = True
                print(f'Inserting {RfD} ...')
                db.session.add(RfD)

    db.session.commit()
    return found_new


def insert_breast_milk_chem_params(df, db):
    from schema import Product, Chemical
    from schema import Parameter
    found_new = False

    s = get_scenario()

    p = Product.query.filter_by(name='breast milk').first()
    if not p:
        print(
            "No product definition was found for breast milk;"
            " unable to load breast milk-chemical parameters"
        )
        return False

    for _, row in df.iterrows():
        chem = Chemical.query.filter_by(
            cas_number=row['CAS'],
            hap_name=row['HAP_Name']
        ).first()
        if not chem:
            print(
                f"Chemical('{row['CAS']}', {row['HAP_Name']}')",
                f"not found for Breast Milk Parameters"
            )
            continue
        v = np.nanmean([(x if x is not None else np.nan) for x in row[2:]])
        if np.isnan(v):
            continue  # No values to add

        params = Parameter.query.filter_by(
            scenario_id=s.id,
            chemical_id=chem.id,
            media_id=p.id
        )

        if row['AE_inf'] is not None:
            AE_inf = params.filter_by(variable='AE_inf').first()
            if not AE_inf:
                AE_inf = Parameter(
                    scenario=s,
                    chemical=chem,
                    media=p,
                    name='infant absorption efficiency',
                    variable='AE_inf',
                    value=row['AE_inf']
                )
                found_new = True
                print(f'Inserting {AE_inf} ...')
                db.session.add(AE_inf)

        if row['AE_mat'] is not None:
            AE_mat = params.filter_by(variable='AE_mat').first()
            if not AE_mat:
                AE_mat = Parameter(
                    scenario=s,
                    chemical=chem,
                    media=p,
                    name='maternal absorption efficiency',
                    variable='AE_mat',
                    value=row['AE_mat']
                )
                found_new = True
                print(f'Inserting {AE_mat} ...')
                db.session.add(AE_mat)

        if row['f_bl'] is not None:
            f_bl = params.filter_by(variable='f_bl').first()
            if not f_bl:
                f_bl = Parameter(
                    scenario=s,
                    chemical=chem,
                    media=p,
                    name='fraction in maternal blood',
                    variable='f_bl',
                    value=row['f_bl']
                )
                found_new = True
                print(f'Inserting {f_bl} ...')
                db.session.add(f_bl)

        if row['f_f'] is not None:
            f_f = params.filter_by(variable='f_f').first()
            if not f_f:
                f_f = Parameter(
                    scenario=s,
                    chemical=chem,
                    media=p,
                    name='fraction in maternal body fat',
                    variable='f_f',
                    value=row['f_f']
                )
                found_new = True
                print(f'Inserting {f_f} ...')
                db.session.add(f_f)

        if row['h_bm'] is not None:
            h_bm = params.filter_by(variable='h_bm').first()
            if not h_bm:
                h_bm = Parameter(
                    scenario=s,
                    chemical=chem,
                    media=p,
                    name='half life',
                    variable='h_bm',
                    value=row['h_bm'],
                    unit='day'
                )
                found_new = True
                print(f'Inserting {h_bm} ...')
                db.session.add(h_bm)

        if row['k_aq_elac'] is not None:
            k_aq_elac = params.filter_by(variable='k_aq_elac').first()
            if not k_aq_elac:
                k_aq_elac = Parameter(
                    scenario=s,
                    chemical=chem,
                    media=p,
                    name='lactating elimination rate constant',
                    variable='k_aq_elac',
                    value=row['k_aq_elac'],
                    unit='day^-1'
                )
                found_new = True
                print(f'Inserting {k_aq_elac} ...')
                db.session.add(k_aq_elac)

        if row['k_elim'] is not None:
            k_elim = params.filter_by(variable='k_elim').first()
            if not k_elim:
                k_elim = Parameter(
                    scenario=s,
                    chemical=chem,
                    media=p,
                    name='non-lactating elimination rate constant',
                    variable='k_elim',
                    value=row['k_elim'],
                    unit='day^-1'
                )
                found_new = True
                print(f'Inserting {k_elim} ...')
                db.session.add(k_elim)

        if row['PC_pl_aq'] is not None:
            PC_pl_aq = params.filter_by(variable='PC_pl_aq').first()
            if not PC_pl_aq:
                PC_pl_aq = Parameter(
                    scenario=s,
                    chemical=chem,
                    media=p,
                    name='blood-milk partition coefficient',
                    variable='PC_pl_aq',
                    value=row['PC_pl_aq'],
                    unit='g/g'
                )
                found_new = True
                print(f'Inserting {PC_pl_aq} ...')
                db.session.add(PC_pl_aq)

        if row['PC_rbc_pl'] is not None:
            PC_rbc_pl = params.filter_by(variable='PC_rbc_pl').first()
            if not PC_rbc_pl:
                PC_rbc_pl = Parameter(
                    scenario=s,
                    chemical=chem,
                    media=p,
                    name='blood cell-plasma partition coefficient',
                    variable='PC_rbc_pl',
                    value=row['PC_rbc_pl'],
                    unit='mL/mL'
                )
                found_new = True
                print(f'Inserting {PC_rbc_pl} ...')
                db.session.add(PC_rbc_pl)

    db.session.commit()
    return found_new


def insert_mutagenic_adjustment_params(df, db):
    from schema import LifeStage, Chemical
    from schema import Parameter
    found_new = False

    s = get_scenario()

    ages = LifeStage.query.all()
    ages = [a for a in ages if a.name != 'Pregnant Mother']

    for _, row in df.iterrows():
        chem = Chemical.query.filter_by(
            cas_number=row['CAS'],
            hap_name=row['HAP_Name']
        ).first()
        if not chem:
            print(
                f"Chemical('{row['CAS']}', {row['HAP_Name']}')",
                f"not found for ADAF Parameters"
            )
            continue
        if not chem.mutagenic:
            continue
        for ls in ages:
            if not ls:
                continue
            params = Parameter.query.filter_by(
                scenario_id=s.id,
                chemical_id=chem.id,
                life_stage_id=ls.id
            )

            ADAF = params.filter_by(variable='ADAF').first()
            if not ADAF:
                try:
                    val = None
                    for name in df.columns.values[1:]:
                        n = name.replace('\n', ' ')
                        n = n.replace('20-70', '')
                        n = n.replace('(', '').replace(')', '')
                        if n.strip() == ls.name:
                            val = row[name]
                            break
                    if not val:
                        raise KeyError
                    if np.isnan(val):
                        continue
                    ADAF = Parameter(
                        scenario=s,
                        chemical=chem,
                        life_stage=ls,
                        name='mutagenic age-dependent adjustment factor',
                        variable='ADAF',
                        value=val
                    )
                except KeyError:
                    print(f'ADAF not found for {chem}, {ls}')
                    continue

                found_new = True
                print(f'Inserting {ADAF} ...')
                db.session.add(ADAF)

    db.session.commit()
    return found_new


def insert_plant_chem_params(df, db):
    from schema import Product, Chemical
    from schema import Parameter
    found_new = False

    s = get_scenario()

    prods = Product.query.all()
    prods = [
        p for p in prods
        if p.is_a('plant') and (p.is_feed or p.is_food)
    ]

    abbrevs = {
        'exposed fruit': 'EF',
        'exposed vegetable': 'EV',
        'forage': 'Fo',
        'grain': 'Gr',
        'protected fruit': 'PF',
        'protected vegetable': 'PV',
        'root': 'Root',
        'silage': 'Si'
    }

    for _, row in df.iterrows():
        chem = Chemical.query.filter_by(
            cas_number=row['CAS'],
            hap_name=row['HAP_Name']
        ).first()
        if not chem:
            print(
                f"Chemical('{row['CAS']}', {row['HAP_Name']}')",
                f"not found for Chemical Parameters"
            )
            continue
        for p in prods:
            params = Parameter.query.filter_by(
                scenario_id=s.id,
                chemical_id=chem.id,
                media_id=p.id
            )

            abbr = abbrevs.get(p.name, p.name)

            if row[abbr + '-Br'] is not None:
                Br = params.filter_by(variable='Br').first()
                if not Br:
                    Br = Parameter(
                        scenario=s,
                        chemical=chem,
                        media=p,
                        name='plant-soil bioconcentration factor',
                        variable='Br',
                        value=row[abbr + '-Br']
                    )
                    found_new = True
                    print(f'Inserting {Br} ...')
                    db.session.add(Br)

            if row[abbr + '-VGag'] is not None:
                VG = params.filter_by(variable='VG').first()
                if not VG:
                    VG = Parameter(
                        scenario=s,
                        chemical=chem,
                        media=p,
                        name='empirical correction factor',
                        variable='VG',
                        value=row[abbr + '-VGag']
                    )
                    found_new = True
                    print(f'Inserting {VG} ...')
                    db.session.add(VG)

            if row['RCF'] is not None:
                RCF = params.filter_by(variable='RCF').first()
                if not RCF:
                    RCF = Parameter(
                        scenario=s,
                        chemical=chem,
                        media=p,
                        name='root concentration factor',
                        variable='RCF',
                        value=row['RCF'],
                        unit='L/kg'
                    )
                    found_new = True
                    print(f'Inserting {RCF} ...')
                    db.session.add(RCF)

            if row['Bvag'] is not None:
                Bv_ag = params.filter_by(variable='Bv_ag').first()
                if not Bv_ag:
                    Bv_ag = Parameter(
                        scenario=s,
                        chemical=chem,
                        media=p,
                        name='air-plant biotransfer factor',
                        variable='Bv_ag',
                        value=row['Bvag']
                    )
                    found_new = True
                    print(f'Inserting {Bv_ag} ...')
                    db.session.add(Bv_ag)

    db.session.commit()
    return found_new


def insert_animal_chem_params(df, db):
    from schema import Product, Chemical
    from schema import Parameter
    found_new = False

    s = get_scenario()

    prods = Product.query.all()
    prods = [
        p for p in prods
        if p.is_a('animal') and (p.is_feed or p.is_food)
    ]

    for _, row in df.iterrows():
        chem = Chemical.query.filter_by(
            cas_number=row['CAS'],
            hap_name=row['HAP_Name']
        ).first()
        if not chem:
            print(
                f"Chemical('{row['CAS']}', {row['HAP_Name']}')",
                f"not found for Chemical Parameters"
            )
            continue
        for p in prods:
            params = Parameter.query.filter_by(
                scenario_id=s.id,
                chemical_id=chem.id,
                media_id=p.id
            )

            try:
                ba = row['Ba_' + p.name.capitalize()]
            except KeyError:
                ba = None
            if ba is not None:
                Ba = params.filter_by(variable='Ba').first()
                if not Ba:
                    Ba = Parameter(
                        scenario=s,
                        chemical=chem,
                        media=p,
                        name='biotransfer factor',
                        variable='Ba',
                        value=ba,
                        unit='day/kg'
                    )
                    found_new = True
                    print(f'Inserting {Ba} ...')
                    db.session.add(Ba)

            # We won't store blanks or 1, because 1 is the assumed
            # default value in the code if no MF is stored
            if row['MF'] is not None and row['MF'] != 1:
                MF = params.filter_by(variable='MF').first()
                if not MF:
                    MF = Parameter(
                        scenario=s,
                        chemical=chem,
                        media=p,
                        name='metabolism factor',
                        variable='MF',
                        value=row['MF']
                    )
                    found_new = True
                    print(f'Inserting {MF} ...')
                    db.session.add(MF)

            # We won't store blanks or 1, because 1 is the assumed
            # default value in the code if no Bs is stored
            if row['Bs'] is not None and row['Bs'] != 1:
                Bs = params.filter_by(variable='Bs').first()
                if not Bs:
                    Bs = Parameter(
                        scenario=s,
                        chemical=chem,
                        media=p,
                        name='soil bioavailability factor',
                        variable='Bs',
                        value=row['Bs']
                    )
                    found_new = True
                    print(f'Inserting {Bs} ...')
                    db.session.add(Bs)

            if p.name == 'fish' and row['FishCookingAdjFactor'] is not None:
                fish_adj = params.filter_by(variable='FishAdjFactor').first()
                if not fish_adj:
                    fish_adj = Parameter(
                        scenario=s,
                        chemical=chem,
                        media=p,
                        name='fish cooking adjustment factor',
                        variable='FishAdjFactor',
                        value=row['FishCookingAdjFactor']
                    )
                    found_new = True
                    print(f'Inserting {fish_adj} ...')
                    db.session.add(fish_adj)

    db.session.commit()
    return found_new


def load_baf_params(df, db):
    df = df.iloc[0:].reset_index(drop=True)

    found_new = []

    baf_df = df.iloc[:, 2:10]
    baf_df = rows_to_headers(baf_df)
    baf_df = baf_df.where((pd.notnull(baf_df)), None)
    if insert_baf_params(baf_df, db):
        found_new.append('Added new B(S)AF Parameter(s)')

    return found_new


def insert_baf_params(df, db):
    from schema import Product, Chemical
    from schema import Parameter
    found_new = False

    s = get_scenario()

    c = Chemical.query.filter_by(hap_name='Arsenic compounds').first()
    if not c:
        print("Default B(S)AFs only apply to Arsenic, which was not found")
        return False

    for t in df.columns.values:
        type_name = t.lower()
        at = Product.query.filter_by(name=type_name).first()
        if not at:
            print(
                f"Product('{type_name}') not found"
            )
            continue
        params = Parameter.query.filter_by(
            scenario_id=s.id,
            chemical_id=c.id,
            media_id=at.id
        )

        if 'benthic' in at.name.lower():
            nm = 'biota sediment accumulation factor'
            var = 'BSAF'
        else:
            nm = 'bioaccumulation factor'
            var = 'BAF'

        BAF = params.filter_by(variable=var).first()
        if not BAF:
            try:
                val = df[t][1]
                u = df[t][0]
                u = u.split('(')[1].split(')')[0]

                BAF = Parameter(
                    scenario=s,
                    chemical=c,
                    media=at,
                    name=nm,
                    variable=var,
                    value=val,
                    unit=u
                )
                if 'ww fish' in BAF.unit or 'dw sediment' in BAF.unit:
                    BAF.notes = BAF.unit
                    BAF.unit = BAF.unit.replace('ww fish', '')
                    BAF.unit = BAF.unit.replace('dw sediment', '')
            except KeyError:
                print(f'{var} not found for {c}, {at}')
                continue

            found_new = True
            print(f'Inserting {BAF} ...')
            db.session.add(BAF)

    db.session.commit()
    return found_new


def load_breast_milk_params(df, db):
    df = df.iloc[185:].reset_index(drop=True)

    found_new = []

    bm_df = df.iloc[:, 15:18]
    bm_df.columns = ['unit', 'var', 'value']
    bm_df = bm_df.where((pd.notnull(bm_df)), None)
    bm_df = bm_df.set_index('var').transpose()
    bm_df = bm_df.drop(columns=[None]).head()
    if insert_breast_milk_params(bm_df, db):
        found_new.append('Added new Breast Milk Parameter(s)')

    return found_new


def insert_breast_milk_params(df, db):
    from schema import Product, LifeStage, Percentile
    from schema import Parameter
    found_new = False

    s = get_scenario()

    p = Product.query.filter_by(name='breast milk').first()
    if not p:
        print(
            "No product definition was found for breast milk;"
            " unable to load breast milk parameters"
        )
        return False

    ls = LifeStage.query.filter_by(name='Child <1').first()
    if not ls:
        print(
            "Default breast milk parameters are only defined for"
            " LifeStage == 'Child <1', which was not found"
        )
        return False

    mat = LifeStage.query.filter_by(name='Pregnant Mother').first()
    if not mat:
        print(
            "Breast Milk calculations require a Body Weight with"
            " LifeStage == 'Pregnant Mother', which was not found"
        )
        return False

    params = Parameter.query.filter_by(
        scenario_id=s.id,
        media_id=p.id
    )

    if df['EF_bm'][1] is not None:
        EF = params.filter_by(variable='EF').first()
        if not EF:
            EF = Parameter(
                scenario=s,
                media=p,
                name='exposure frequency',
                variable='EF',
                value=df['EF_bm'][1],
                unit='day'
            )
            found_new = True
            print(f'Inserting {EF} ...')
            db.session.add(EF)

    if df['f_mbm'][1] is not None:
        f_mbm = params.filter_by(variable='f_mbm').first()
        if not f_mbm:
            f_mbm = Parameter(
                scenario=s,
                media=p,
                name='fraction breast milk fat',
                variable='f_mbm',
                value=df['f_mbm'][1]
            )
            found_new = True
            print(f'Inserting {f_mbm} ...')
            db.session.add(f_mbm)

    if df['f_fm'][1] is not None:
        f_fm = params.filter_by(variable='f_fm').first()
        if not f_fm:
            f_fm = Parameter(
                scenario=s,
                media=p,
                name='fraction maternal weight fat',
                variable='f_fm',
                value=df['f_fm'][1]
            )
            found_new = True
            print(f'Inserting {f_fm} ...')
            db.session.add(f_fm)

    if df['t_pn'][1] is not None:
        t_pn = params.filter_by(variable='t_pn').first()
        if not t_pn:
            t_pn = Parameter(
                scenario=s,
                media=p,
                name='maternal exposure duration',
                variable='t_pn',
                value=df['t_pn'][1],
                unit='day'
            )
            found_new = True
            print(f'Inserting {t_pn} ...')
            db.session.add(t_pn)

    if df['f_pm'][1] is not None:
        f_pm = params.filter_by(variable='f_pm').first()
        if not f_pm:
            f_pm = Parameter(
                scenario=s,
                media=p,
                name='fraction maternal weight plasma',
                variable='f_pm',
                value=df['f_pm'][1]
            )
            found_new = True
            print(f'Inserting {f_pm} ...')
            db.session.add(f_pm)

    pct = Percentile.query.filter_by(name='P90').first()

    if df['IR_bm'][1] is not None:
        IR = Parameter.query.filter_by(
            variable='IR',
            food_id=p.id,
            percentile_id=pct.id,
            life_stage_id=ls.id
        ).first()
        if not IR:
            IR = Parameter(
                scenario=s,
                food=p,
                percentile=pct,
                life_stage=ls,
                name='ingestion rate',
                variable='IR',
                value=df['IR_bm'][1],
                unit=df['IR_bm'][0]
            )
            found_new = True
            print(f'Inserting {IR} ...')
            db.session.add(IR)

    pct = Percentile.query.filter_by(name='Pmean').first()

    if df['BW_mat'][1] is not None:
        BW = Parameter.query.filter_by(
            variable='BW',
            percentile_id=pct.id,
            life_stage_id=mat.id
        ).first()
        if not BW:
            BW = Parameter(
                scenario=s,
                percentile=pct,
                life_stage=mat,
                name='body weight',
                variable='BW',
                value=df['BW_mat'][1],
                unit=df['BW_mat'][0]
            )
            found_new = True
            print(f'Inserting {BW} ...')
            db.session.add(BW)

    db.session.commit()
    return found_new


def load_exposure_params(df, db):
    df.replace('-', np.nan, inplace=True)
    df = df.iloc[6:].reset_index(drop=True)

    found_new = []

    loss_df = df.iloc[:, 1:8]
    loss_df = rows_to_headers(loss_df)
    loss_df = loss_df.where((pd.notnull(loss_df)), None)
    if insert_prod_params(loss_df, db):
        found_new.append('Added new Product Parameter(s)')

    animal_ir_df = df.iloc[:, 9:15]
    animal_ir_df = rows_to_headers(animal_ir_df)
    animal_ir_df = animal_ir_df.where((pd.notnull(animal_ir_df)), None)
    if insert_animal_ir_params(animal_ir_df, db):
        found_new.append('Added new Animal Ingestion Rate Parameter(s)')

    plant_df = df.iloc[:, 16:23]
    plant_df = rows_to_headers(plant_df)
    plant_df = plant_df.where((pd.notnull(plant_df)), None)
    if insert_plant_params(plant_df, db):
        found_new.append('Added new Plant Product Parameter(s)')

    bw_df = df.iloc[:, 24:31]
    bw_df = rows_to_headers(bw_df)
    bw_df = bw_df.where((pd.notnull(bw_df)), None)
    if insert_bw_params(bw_df, db):
        found_new.append('Added new Body Weight Parameter(s)')

    non_fish_ir_df = df.iloc[:, 32:39]
    non_fish_ir_df = rows_to_headers(non_fish_ir_df)
    non_fish_ir_df = non_fish_ir_df.where((pd.notnull(non_fish_ir_df)), None)
    if insert_non_fish_ir_params(non_fish_ir_df, db):
        found_new.append('Added new Non-Fish Ingestion Rate Parameter(s)')

    fish_ir_df = df.iloc[:, 40:48]
    fish_ir_df = rows_to_headers(fish_ir_df)
    fish_ir_df = fish_ir_df.where((pd.notnull(fish_ir_df)), None)
    if insert_fish_ir_params(fish_ir_df, db):
        found_new.append('Added new Fish Ingestion Rate Parameter(s)')

    return found_new


def insert_prod_params(df, db):
    from schema import Product
    from schema import Parameter
    found_new = False

    df = df.dropna(subset=['Product'])

    s = get_scenario()

    for _, row in df.iterrows():
        p_name = row['Product'].lower()
        p = Product.query.filter_by(name=p_name).first()
        if not p:
            print(
                f"Product('{p_name}') not found",
                f"for 'Loss Type #1', 'Loss Type #2'"
            )
            continue

        params = Parameter.query.filter_by(
            scenario_id=s.id,
            media_id=p.id
        )

        if row['Exposure Factor'] is not None:
            EF = params.filter_by(variable='EF').first()
            if not EF:
                EF = Parameter(
                    scenario=s,
                    media=p,
                    name='exposure frequency',
                    variable='EF',
                    value=row['Exposure Factor'],
                    unit='day'
                )
                found_new = True
                print(f'Inserting {EF} ...')
                db.session.add(EF)

        if row['Fraction Exposed'] is not None:
            FC = params.filter_by(variable='FC').first()
            if not FC:
                FC = Parameter(
                    scenario=s,
                    media=p,
                    name='fraction contaminated',
                    variable='FC',
                    value=row['Fraction Exposed']
                )
                found_new = True
                print(f'Inserting {FC} ...')
                db.session.add(FC)

        loss_names = ['Loss Type #1', 'Loss Type #2']
        for loss_name in loss_names:
            if row[loss_name] is not None:
                LF = params.filter_by(name=loss_name, variable='LF').first()
                if not LF:
                    LF = Parameter(
                        scenario=s,
                        media=p,
                        name=loss_name,
                        variable='LF',
                        value=row[loss_name],
                        source=row['Loss Source']
                    )
                    found_new = True
                    print(f'Inserting {LF} ...')
                    db.session.add(LF)

    db.session.commit()
    return found_new


def insert_bw_params(df, db):
    from schema import LifeStage, Percentile
    from schema import Parameter
    found_new = False

    df = df.dropna(subset=['BW_Percentile'])

    ages = [
        LifeStage.query.filter_by(name=age).first()
        for age in df.columns.values[1:]
    ]

    default_scenario = get_scenario()

    for _, row in df.iterrows():
        pct_name = row[0].split()[0].strip()
        pct = Percentile.query.filter_by(name=pct_name).first()
        if not pct:
            print(
                f"Percentile('{pct_name}') not found",
                f"for Body Weight Parameters"
            )
            continue
        if '80kg Adult' in row[0]:
            s = get_scenario('RTR Screening')
        else:
            s = default_scenario

        params = Parameter.query.filter_by(
            scenario_id=s.id,
            variable='BW'
        )
        for ls in ages:
            if not ls:
                continue
            if '80kg Adult' in row[0] and ls.name != 'Adult':
                continue

            if row[ls.name] is not None:
                BW = params.filter_by(
                    life_stage_id=ls.id,
                    percentile_id=pct.id
                ).first()
                if not BW:
                    BW = Parameter(
                        scenario=s,
                        life_stage=ls,
                        percentile=pct,
                        name='body weight',
                        variable='BW',
                        value=row[ls.name],
                        unit='kg'
                    )
                    found_new = True
                    print(f'Inserting {BW} ...')
                    db.session.add(BW)

    db.session.commit()
    return found_new


def insert_plant_params(df, db):
    from schema import Product
    from schema import Parameter
    found_new = False

    df = df.dropna(subset=['PlantPart'])

    s = get_scenario()

    for _, row in df.iterrows():
        p_name = row[0].strip().lower()
        p_name = p_name.replace('exp', 'exposed ')
        p_name = p_name.replace('prot', 'protected ')
        p_name = p_name.replace('veg', 'vegetable')
        p = Product.query.filter_by(name=p_name).first()
        if not p:
            print(
                f"Product('{p_name}') not found",
                f"for Plant Parameters"
            )
            continue
        if not p.is_a('plant'):
            print(
                f"Product('{p_name}') is not a plant or feed,",
                f"but has Plant Parameters"
            )
            continue

        params = Parameter.query.filter_by(
            scenario_id=s.id,
            media_id=p.id
        )

        if row['Rp'] is not None:
            Rp = params.filter_by(variable='Rp').first()
            if not Rp:
                Rp = Parameter(
                    scenario=s,
                    media=p,
                    name='interception fraction',
                    variable='Rp',
                    value=row['Rp']
                )
                found_new = True
                print(f'Inserting {Rp} ...')
                db.session.add(Rp)

        if row['kp'] is not None:
            kp = params.filter_by(variable='kp').first()
            if not kp:
                kp = Parameter(
                    scenario=s,
                    media=p,
                    name='surface loss coefficient',
                    variable='kp',
                    value=row['kp'],
                    unit='year^-1'
                )
                found_new = True
                print(f'Inserting {kp} ...')
                db.session.add(kp)

        if row['Tp'] is not None:
            Tp = params.filter_by(variable='Tp').first()
            if not Tp:
                Tp = Parameter(
                    scenario=s,
                    media=p,
                    name='exposure length',
                    variable='Tp',
                    value=row['Tp'],
                    unit='year'
                )
                found_new = True
                print(f'Inserting {Tp} ...')
                db.session.add(Tp)

        if row['Yp'] is not None:
            Yp = params.filter_by(variable='Yp').first()
            if not Yp:
                Yp = Parameter(
                    scenario=s,
                    media=p,
                    name='standing biomass',
                    variable='Yp',
                    value=row['Yp'],
                    unit='kg/m^2'
                )
                found_new = True
                print(f'Inserting {Yp} ...')
                db.session.add(Yp)

        if row['MAF'] is not None:
            MAF = params.filter_by(variable='MAF').first()
            if not MAF:
                MAF = Parameter(
                    scenario=s,
                    media=p,
                    name='moisture adjustment factor',
                    variable='MAF',
                    # round to avoid floating-point division errors
                    value=round(
                        row['MAF'] / 100.0,
                        len(str(row['MAF']).replace('.', ''))
                    )
                )
                found_new = True
                print(f'Inserting {MAF} ...')
                db.session.add(MAF)

    db.session.commit()
    return found_new


def insert_animal_ir_params(df, db):
    from schema import Product
    from schema import Parameter
    found_new = False

    df = df.dropna(subset=['AnimalProduct'])

    s = get_scenario()

    for _, row in df.iterrows():
        c_name = row[0].strip().lower()
        c = Product.query.filter_by(name=c_name).first()
        if not c:
            print(
                f"Product('{c_name}') not found",
                f"for Animal Ingestion Rate Parameters"
            )
            continue
        if not c.is_a('animal'):
            print(
                f"Product('{c_name}') is not an animal or fish,",
                f"but has Animal Ingestion Rate Parameters"
            )
            continue
        p_name = row[1].strip().lower()
        p_names = ['soil', p_name]

        params = Parameter.query.filter_by(
            scenario_id=s.id,
            media_id=c.id
        )
        for name in p_names:
            p = Product.query.filter_by(name=name).first()
            if not p:
                print(
                    f"Product('{name}') not found",
                    f"for Animal Ingestion Rate Parameters"
                )
                continue

            if row['Qp'] is not None or row['Qs'] is not None:
                IR = params.filter_by(
                    food_id=p.id, variable='IR'
                ).first()
                if not IR:
                    if name == 'soil':
                        ir = row['Qs']
                    else:
                        ir = row['Qp']
                    IR = Parameter(
                        scenario=s,
                        media=c,
                        food=p,
                        name='ingestion rate',
                        variable='IR',
                        value=ir,
                        unit='kg/day'
                    )
                    found_new = True
                    print(f'Inserting {IR} ...')
                    db.session.add(IR)

            if row['F'] is not None:
                FC = Parameter.query.filter_by(
                    scenario_id=s.id,
                    media_id=p.id,
                    variable='FC'
                ).first()
                if not FC:
                    FC = Parameter(
                        scenario=s,
                        media=p,
                        name='fraction contaminated',
                        variable='FC',
                        value=row['F']
                    )
                    found_new = True
                    print(f'Inserting {FC} ...')
                    db.session.add(FC)

    db.session.commit()
    return found_new


def insert_non_fish_ir_params(df, db):
    from schema import LifeStage, Percentile, Product
    from schema import Parameter
    found_new = False

    df = df.dropna(subset=['Species, Ingestion Group, Percentile'])

    ages = [
        LifeStage.query.filter_by(name=age).first()
        for age in df.columns.values[1:]
    ]

    pcts = Percentile.query.all()
    none_pct = Percentile.query.filter_by(name='None').first()

    s = get_scenario()  # default
    s_screening = get_scenario('RTR Screening')  # RTR

    for _, row in df.iterrows():
        idx = row[0].lower().split()
        # Get the product this row refers to
        p_name = idx[0]
        p_name = p_name.replace('exp', 'exposed ')
        p_name = p_name.replace('prot', 'protected ')
        p_name = p_name.replace('veg', 'vegetable')
        p = Product.query.filter_by(name=p_name).first()
        if not p:
            print(
                f"Product('{p_name}') not found",
                f"for Human Ingestion Rate Parameters"
            )
            continue

        # Get the percentile this row refers to (default 'none')
        if sum([(x if x is not None else np.nan) for x in row[1:7]]) == 0:
            pct = none_pct  # Force 0s to be None
        else:
            pct = [pct for pct in pcts if pct.name.lower() in idx]
            pct.append(none_pct)
            pct = pct[0]

        # Don't load anything from RTR-specific scenarios; these
        # are just duplicates of specific percentiles!!
        if 'rtr' in idx and 'f' in idx:
            continue  # s = get_scenario('RTR Farmer EFH11')
        if 'rtr' in idx and 'ug' in idx:
            continue  # s = get_scenario('RTR Urban Gardener EFH11')
        if 'rtr' in idx and 'rg' in idx:
            continue  # s = get_scenario('RTR Rural Gardener EFH11')
        if 'rtr' in idx and 'fish' in idx and 'only' in idx:
            continue  # s = get_scenario('RTR Fisher EFH11')

        params = Parameter.query.filter_by(
            scenario_id=s.id,
            food_id=p.id,
            percentile_id=pct.id
        )

        # Cycle through the lifestages to get values
        for ls in ages:
            if not ls:
                continue
            ir = row[ls.name]
            if ir is None:
                continue

            IR = params.filter_by(
                scenario_id=s.id,
                life_stage_id=ls.id,
                variable='IR'
            ).first()
            if not IR:
                IR = Parameter(
                    scenario=s,
                    food=p,
                    percentile=pct,
                    life_stage=ls,
                    name='ingestion rate',
                    variable='IR',
                    value=ir
                )
                if p.name == 'water':
                    IR.unit = 'mL/day'
                    IR.notes = (
                        "Human water ingestion rates set to mL/day,"
                        " as in MIRC Appendix B"
                    )
                elif p.name == 'soil':
                    IR.unit = 'mg/day'
                    IR.notes = (
                        "Human soil ingestion rates set to mg/day,"
                        " as in MIRC Appendix B"
                    )
                else:
                    IR.unit = 'g/day/kg'
                    IR.notes = (
                        "General human ingestion rates set to g/day/kg,"
                        " as in MIRC Appendix B"
                    )
                found_new = True
                print(f'Inserting {IR} ...')
                db.session.add(IR)

            if '<1' in ls.name:
                IR = params.filter_by(
                    scenario_id=s_screening.id,
                    life_stage_id=ls.id,
                    variable='IR'
                ).first()
                if not IR:
                    IR = Parameter(
                        scenario=s_screening,
                        food=p,
                        percentile=pct,
                        life_stage=ls,
                        name='ingestion rate',
                        variable='IR',
                        value=0,
                        notes=(
                            'Infant ingestion of non-breast milk products'
                            ' is assumed to be 0 for RTR'
                        )
                    )
                    if p.name == 'water':
                        IR.unit = 'mL/day'
                    elif p.name == 'soil':
                        IR.unit = 'mg/day'
                    else:
                        IR.unit = 'g/day/kg'
                    found_new = True
                    print(f'Inserting {IR} ...')
                    db.session.add(IR)

    db.session.commit()
    return found_new


def insert_fish_ir_params(df, db):
    from schema import LifeStage, Percentile, Product
    from schema import Parameter
    found_new = False

    df = df.dropna(subset=['Species, Ingestion Group, Percentile'])

    s = get_scenario()

    ages = [
        LifeStage.query.filter_by(name=age).first()
        for age in df.columns.values[1:]
    ]

    pct = Percentile.query.filter_by(name='P99').first()
    p = Product.query.filter_by(name='fish').first()

    params = Parameter.query.filter_by(
        scenario_id=s.id,
        food_id=p.id,
        percentile_id=pct.id
    )

    for _, row in df.iterrows():
        # Get the percentile this row refers to (default 'P99')
        if sum([(x if x is not None else np.nan) for x in row[1:7]]) == 0:
            continue

        # Cycle through the lifestages to get values
        for ls in ages:
            if not ls:
                continue
            ir = row[ls.name]
            if ir is None:
                continue

            IR = params.filter_by(
                life_stage_id=ls.id,
                variable='IR'
            ).first()
            if not IR:
                IR = Parameter(
                    scenario=s,
                    food=p,
                    percentile=pct,
                    life_stage=ls,
                    name='ingestion rate',
                    variable='IR',
                    value=ir,
                    unit='g/day',
                    notes=(
                        "Fish ingestion rates set to g/day,"
                        " as in MIRC Appendix B"
                    ),
                    source=row['Sources/Notes']
                )
                found_new = True
                print(f'Inserting {IR} ...')
                db.session.add(IR)

    db.session.commit()
    return found_new

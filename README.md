# PySMIRC Database

These files define the database structure for PySMIRC,
and also provide utilities for testing and creating
local copies of the basic database structure.

## Directory Structure

- **pysmirc_db**
    - **core**
        - *scripts containing the MIRC pathway functions*
    - **data**
        - *scripts for loading data from Excel MIRC into the db*
    - **demos**
        - *scripts that demonstrate how to create and access the db*
    - **migrations**
        - *scripts for migrating the db automatically*
    - **schema**
        - *scripts containing the ORM classes that define the db structure*
    - **utils**
        - *helper scripts for running demos and generating migrations*
    - README.md
    - requirements.txt  *(defines python dependencies)*
    - Pipfile  *(an alternative way of defining python dependencies)*
    - run_demo.py  *(executes the demo scripts)*
    - alembic.ini  *(configuration for generating migration scripts)*
    - SMIRC DB Schema.docx  *(db schema definition & explanations)*
    - RTR_TSD_Appendix_B_MIRC.pdf  *(source for MIRC equations & definitions)*

## Demos

There are 2 demos included along with the basic schema definition files:

1. *create_db.py* - This demo shows how you can load parameter data
from Excel MIRC into a new database using the functions in the data folder.
2. *run_pathways.py* - This demo shows how you can use the ORM
(object-relational-model) version of a scenario to run the
RTR MIRC pathways

To run the demos, call the *run_demo.py* script in the root folder.
You must provide flags to indicate which demo you want to run.
The flags are:

1. `--create`
2. `--pathways`

For example, to run *create_db.py*, you would execute the following
command:

``` python run_demo.py --create ```

You can run multiple demos in sequence by passing multiple flags
(though they will always run in the above order, regardless of
the order you pass the flags in).

**NOTE:** Due to the structure of imports in this project, you *cannot*
call the demo scripts directly.  You must use the *run_demo.py* script
with the appropriate flag(s).

## Developer Notes

### Including the ORM in an Application

When including the database ORM in a PySMIRC application
(be it web-based or local), you do not need all of the files
in this project.  Instead, you should copy over the following
folders and files:

- **pysmirc_db**
    - **data**
        - *optional -- see "Including the Data-Loading Functions" below*
    - **migrations**
        - **versions**
            - *all files*
        - env.py
    - **schema**
    - \_\_init\_\_.py
    - alembic.ini

You can then import all ORM models (including the Model base class)
directly from the `pysmirc_db` project.

NOTE: The **demos** and **utils** folders are not intended to ever
be copied directly into another project, though you may wish to
adapt/copy some of the individual demo functions.

#### Including the Data-Loading Functions

If you also wish to include the data-loading functions
(either just the defaults, or also the parameter loader),
you should copy the **data** directory over as well
(as above).

Note that you *must* modify any functions you want to use
before calling them from an external application, because
the Python module scope will be different if an application
is using `pysmirc_db` as a package, compared to the scope
established when running one of the testing scripts (such as
create.py) present in the full PySMIRC Database project.

Specifically, you must change *all* `import schema`
or `from schema import` statements in the **data** directory
to `import pysmirc_db` and `from pysmirc_db import`, respectively.
As long as you have copied the folder structure above completely
into your application project, this should resolve the scoping issues.

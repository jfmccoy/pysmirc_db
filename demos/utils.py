import json
import os
import pandas as pd
from bs4 import BeautifulSoup

ROOT = os.path.abspath(os.path.dirname(__file__))


def load_inputs(filename):
    """Loads fake user input data
    """
    with open(os.path.join(ROOT, 'static', filename)) as f:
        fake_inputs = json.load(f)

    return fake_inputs


def parse_user_inputs(user_inputs):
    """
    Extracts input values from faked data; returns a dictionary
    containing the following values:
        'scenario'
        'chemical'
        't3_intake'
        't4_intake'
        'factor_type'
        + the output from parse_trim_inputs
    """

    # Get TRIM Data

    chemical = user_inputs['chemical']

    trim_dir = os.path.join(ROOT, 'static', user_inputs['html_location'])

    trim_vals = parse_trim_inputs(
        trim_dir,
        trim_scenario=user_inputs['trim_scenario'],
        scenario_suffix=user_inputs['trim_suffix'],
        chemical=chemical,
        air_compartment=user_inputs['air_conc'],
        surface_water_compartment=user_inputs['surface_water_conc'],
        sediment_compartment=user_inputs['sediment_conc'],
        soil_compartment=user_inputs['surface_soil_conc_animal'],
        root_compartment=user_inputs['root_soil_conc_animal'],
        dry_dep_compartment=user_inputs['dry_deposition'],
        wet_dep_compartment=user_inputs['wet_deposition'],
        t3_fish_compartment=user_inputs['t3_fish_conc'],
        t4_fish_compartment=user_inputs['t4_fish_conc']
    )

    if chemical == 'arsenic':
        chemical = "Arsenic compounds"
    elif chemical == 'Benzo_A_Pyrene':
        chemical = 'Benzo[a]pyrene'

    input_vals = {
        'scenario': user_inputs['mirc_scenario'],
        'chemical': chemical,
        't3_intake': float(user_inputs['t3_intake']),
        't4_intake': float(user_inputs['t4_intake']),
        'factor_type': user_inputs['factor_type'],
        **trim_vals
    }
    return input_vals


def get_trim_chemical_name(chemical):
    if chemical == 'Arsenic compounds':
        return 'Arsenic'

    elif chemical == 'Methyl Mercury':
        return 'MethylMercury'

    elif chemical == 'Benzo[a]pyrene':
        return ('Benzo_A_Pyrene', 'Benzo(A)Pyrene')

    elif chemical == '2,3,7,8-Tetrachlorodibenzo-p-dioxin':
        return ('2_3_7_8_TCDD', '2,3,7,8-TCDD')

    return chemical


def parse_trim_inputs(
    trim_dir, trim_scenario, scenario_suffix,
    chemical,
    air_compartment,
    surface_water_compartment, sediment_compartment,
    soil_compartment, root_compartment,
    dry_dep_compartment, wet_dep_compartment,
    t3_fish_compartment, t4_fish_compartment
):
    """
    Extracts input values from TRIM data; returns a dictionary
    containing the following values:
        'scenario'
        'chemical'
        'product'
        't3_intake'
        't4_intake'
        'factor_type'
        + the output from parse_trim_inputs
    """

    from schema.parameters.models import ureg

    # ===============================================================
    # REAL TRIM PARSING
    # ===============================================================

    html_dir = os.path.join(trim_dir, 'HTML')

    with open(os.path.join(html_dir, 'CompartmentIndex.html'), 'r') as f:
        soup = BeautifulSoup(f.read(), 'html.parser')

    # Manipulate chemical name if needed
    query = get_trim_chemical_name(chemical)
    if isinstance(query, tuple):
        trim_fname = query[0]
        query = query[1]
    else:
        trim_fname = query

    # Air Data file
    air_html = soup.find('a', text=f'Air in {air_compartment}')['href']
    with open(os.path.join(html_dir, air_html), 'r') as f:
        tables = pd.read_html(f.read())
        const_air_props = tables[2]
        uneven_air_props = tables[3]

    # Fraction of airborne chemical in vapor phase
    Fv = float(uneven_air_props[
        (uneven_air_props['Chemical'] == query) &
        (uneven_air_props['Name'] == 'FractionMass_vapor')
    ]['Value at t'].values[0])

    # Density of air
    rho_a = float(const_air_props[
        const_air_props['Name'] == 'AirDensity_g_cm3'
    ]['Value'])

    try:
        rho_a_u = ureg(str(
            const_air_props[
                const_air_props['Name'] == 'AirDensity_g_cm3'
            ].reset_index()['Units'][0]
        ).replace('g/cm3', 'g/cm^3'))
    except Exception as e:
        print(e)
        rho_a_u = ureg('g/cm^3')
    rho_a = (rho_a * rho_a_u).to('g/m^3')

    # Fraction of total chemical mass in compartment that is
    # dissolved in the liquid phase (i.e., in water)
    FMD = 0
    if surface_water_compartment:
        # Water Data file
        water_html = soup.find('a', text=surface_water_compartment)['href']
        with open(os.path.join(html_dir, water_html), 'r') as f:
            tables1 = pd.read_html(f.read())
            uneven_water_props = tables1[3]

        try:
            FMD = float(uneven_water_props[
                (uneven_water_props['Name'] == 'FractionMass_Dissolved') &
                (uneven_water_props['Chemical'] == query)
            ]['Value at t'])
        except Exception:
            FMD = 0

    # ===============================================================
    # FAKE TRIM PARSING
    # ===============================================================

    # load fake trim data
    with open(os.path.join(ROOT, 'static', f'{trim_dir}.json')) as f:
        fake_trim = json.load(f)
    conc_csv = (
        f"{trim_scenario}_{trim_fname}" +
        f"_conc{scenario_suffix}_annual.txt"
    )
    dep_csv = (
        f"{trim_scenario}_{trim_fname}" +
        f"_dep{scenario_suffix}_annual.txt"
    )
    print(dep_csv)
    conc_by_date = None
    dep_by_date = None
    for k, v in fake_trim.items():
        if k == conc_csv:
            conc_by_date = pd.DataFrame(fake_trim[k])
        elif k == dep_csv:
            dep_by_date = pd.DataFrame(fake_trim[k])

    if conc_by_date is None:
        raise IOError('Unable to load conc_by_date')
    if dep_by_date is None:
        raise IOError('Unable to load dep_by_date')

    # Average chemical concentration in soil for livestock
    Cs_s = float(conc_by_date[
        f'Soil - Surface in {soil_compartment}'  # MAKE THIS CUSTOMIZABLE LATER
    ]) * ureg('mg/kg')

    # Average chemical concentration in soil at root-zone depth
    Cs_root_zone = float(conc_by_date[
        f'Soil - Surface in {root_compartment}'  # MAKE THIS CUSTOMIZABLE LATER
    ]) * ureg('mg/kg')

    # Average annual total chemical concentration in air
    Ca = float(conc_by_date[
        f"Air in {air_compartment}"
    ]) * ureg('ug/m^3')

    # Average chemical concentration in drinking water
    C_water = 0 * ureg('g/L')

    # Chemical concentration in whole fish for trophic level 3.5 (TL3) fish on
    # a wet-weight (WW) basis (mg/kg WW)
    t3_fish_conc = float(conc_by_date[
        f"{t3_fish_compartment}"
    ]) * ureg('mg/kg')

    # Chemical concentration in whole fish for trophic level 3.5 (TL3) fish on
    # a wet-weight (WW) basis (mg/kg WW)
    t4_fish_conc = float(conc_by_date[
        f"{t4_fish_compartment}"
    ]) * ureg('mg/kg')

    # Average annual dry deposition of particle-phase chemical
    # DRDP and DRWP do not need to be multiplied by 365 even though the
    # documentation states it does - ARK 1/13/20
    Drdp = float(dep_by_date[
        f'Dry Particle Deposition to Soil - Surface in {dry_dep_compartment}'
    ]) * ureg('(g/m^2)/day')
    Drdp = Drdp.to('(g/m^2)/year')  # convert daily to annual

    # Average annual wet deposition of particle-phase chemical
    Drwp = float(dep_by_date[
        f'Wet Particle Deposition to Soil - Surface in {wet_dep_compartment}'
    ]) * ureg('(g/m^2)/day')
    Drwp = Drwp.to('(g/m^2)/year')  # convert daily to annual

    # Chemical concentration in sediment (mg/kg WW)
    try:
        sediment_conc = float(conc_by_date[
            f"{sediment_compartment}"
        ]) * ureg('mg/kg')
    except Exception:
        sediment_conc = 0 * ureg('mg/kg')

    # Chemical concentration in surface water phase (mg/L WW)
    try:
        surface_water_conc = float(conc_by_date[
            f"{surface_water_compartment}"
        ]) * ureg('mg/L')
    except Exception:
        surface_water_conc = 0 * ureg('mg/L')

    trim_vals = {
        'Fv': Fv,
        'rho_a': rho_a,
        'FMD': FMD,
        'Cs_s': Cs_s,
        'C_soil': Cs_s,
        'Cs_root_zone': Cs_root_zone,
        'C_root_veg': Cs_root_zone,
        'Ca': Ca,
        'C_water': C_water,
        'Drdp': Drdp,
        'Drwp': Drwp,
        't3_fish_conc': t3_fish_conc,
        't4_fish_conc': t4_fish_conc,
        'sediment_conc': sediment_conc,
        'surface_water_conc': surface_water_conc
    }
    return trim_vals


def make_assessment(input_vals):
    from schema import Chemical, Product, Percentile
    from schema import Assessment
    from schema import Simulation, SimulationPercentile, \
        SimulationParameter, SimulationConsumptionBreakdown

    c = Chemical.query.filter_by(
        hap_name=input_vals.get("chemical")
    ).first()

    a = Assessment(name='Test Assessment')
    s = Simulation(
        name='Simulation 1',
        chemical=c
    )

    if input_vals.get('factor_type') == 'BAF/BSAF':
        s.use_baf = True

    p99 = Percentile.query.filter_by(name='P99').first()
    p90 = Percentile.query.filter_by(name='P90').first()
    p_mean = Percentile.query.filter_by(name='Pmean').first()

    s.percentiles.append(
        SimulationPercentile(percentile=p_mean)
    )

    prods = Product.query.all()
    for p in prods:
        if not p.is_food:
            continue

        if p.name == 'fish':
            pct = p99
        elif p.name == 'water':
            pct = p_mean
        else:
            pct = p90

        s.percentiles.append(
            SimulationPercentile(food=p, percentile=pct)
        )

    for p in prods:
        if not p.is_a('fish') or p.name == 'fish':
            continue

        if p.name == 'benthic carnivore':
            f = input_vals.get('t3_intake')
        elif p.name == 'water column carnivore':
            f = input_vals.get('t4_intake')
        else:
            f = 0

        s.consumption_breakdowns.append(
            SimulationConsumptionBreakdown(subfood=p, fraction=f)
        )

    s.parameters.append(SimulationParameter(
        variable='Ca',
        value=input_vals.get('Ca').magnitude,
        unit=str(input_vals.get('Ca').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='Fv',
        value=input_vals.get('Fv')
    ))

    s.parameters.append(SimulationParameter(
        variable='rho_a',
        value=input_vals.get('rho_a').magnitude,
        unit=str(input_vals.get('rho_a').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='C_water',
        value=input_vals.get('C_water').magnitude,
        unit=str(input_vals.get('C_water').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='C_soil',
        value=input_vals.get('C_soil').magnitude,
        unit=str(input_vals.get('C_soil').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='C_root_veg',
        value=input_vals.get('C_root_veg').magnitude,
        unit=str(input_vals.get('C_root_veg').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='Cs_s',
        value=input_vals.get('Cs_s').magnitude,
        unit=str(input_vals.get('Cs_s').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='Cs_root_zone',
        value=input_vals.get('Cs_root_zone').magnitude,
        unit=str(input_vals.get('Cs_root_zone').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='Drdp',
        value=input_vals.get('Drdp').magnitude,
        unit=str(input_vals.get('Drdp').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='Drwp',
        value=input_vals.get('Drwp').magnitude,
        unit=str(input_vals.get('Drwp').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='C_benthic_carnivore',
        value=input_vals.get('t3_fish_conc').magnitude,
        unit=str(input_vals.get('t3_fish_conc').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='C_water_column_carnivore',
        value=input_vals.get('t4_fish_conc').magnitude,
        unit=str(input_vals.get('t4_fish_conc').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='C_sed',
        value=input_vals.get('sediment_conc').magnitude,
        unit=str(input_vals.get('sediment_conc').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='C_surf_water',
        value=input_vals.get('surface_water_conc').magnitude,
        unit=str(input_vals.get('surface_water_conc').units)
    ))

    s.parameters.append(SimulationParameter(
        variable='FMD',
        value=input_vals.get('FMD')
    ))

    a.simulations.append(s)

    return a

import os
from utils.users_roles import implement_users_roles

ROOT = os.path.abspath(os.path.dirname(__file__))

DB_URI = f'sqlite:///{ROOT}/pysmirc.db'

try:
    Role, User = implement_users_roles()
except Exception as e:
    print(f'Error loading Users/Roles: {e}')


def make_db():
    from utils.engine import DataBase

    db = DataBase(DB_URI)
    return db

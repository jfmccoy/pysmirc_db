from core import *
from schema import Scenario, Product, Percentile
from .utils import *

"""
This demo shows how you can use the ORM (object-relational-model)
version of a scenario to run the RTR MIRC pathways.
"""

__all__ = ['run_pathways']

FAKE_INPUT_FILE = 'methyl_mercury_inputs.json'


def run_pathways(with_breast_milk=True):
    print("\n================== INITIALIZING DB ==================\n")

    # Here, we use a custom db engine that enables us to query ORM
    # models directly. This step would already be taken care of
    # if we were in a Flask application (via Flask-SqlAlchemy),
    # but for a stand-alone demo we must do it ourselves.
    from .setup import make_db
    db = make_db()

    results = []

    print(f"\n============== LOADING 'USER' INPUTS ==============\n")

    # This section loads fake user/TRIM inputs

    user_inputs = load_inputs(FAKE_INPUT_FILE)
    input_vals = parse_user_inputs(user_inputs)

    print('\nExtracted variables from inputs:')
    printvals(**input_vals)

    assessment = make_assessment(input_vals)

    print("\n==================== QUERYING DB ====================\n")

    # This section gives an example of how to query the schema models

    SCENARIO_NAME = input_vals.get("scenario")

    # Read the scenario definition
    s = db.session.query(Scenario).filter_by(name=SCENARIO_NAME).first()
    print(f'Loaded {s}')

    products = Product.query.all()
    products = [
        p for p in products
        if p.is_food and p.name != 'breast milk' and
        s.parameters.for_food(p).IR
    ]
    bm = Product.query.filter_by(name='breast milk').first()
    with_breast_milk = (s.parameters.for_food(bm).IR or False)

    results = []

    Pmean = Percentile.query.filter_by(name='Pmean').first()

    for simulation in assessment.simulations:
        c = simulation.chemical
        print(f'Loaded {c}')

        bw = [
            pct for pct in simulation.percentiles
            if pct.food is None
        ]
        if bw:
            bw = bw[0].percentile
        else:
            bw = Pmean

        simulation_breakdown = {}

        for p in products:
            print(f"\n======== CALCULATING {p.name} CONCENTRATION ========\n")

            c_product = calculate_c_product(
                scenario=s, product=p, chemical=c,
                simulation=simulation
            )

            short_name = p.name.lower().replace(' ', '_')
            short_name = short_name.replace('exposed', 'exp')
            short_name = short_name.replace('protected', 'prot')
            short_name = short_name.replace('vegetable', 'veg')

            print(f"Calculated c_{short_name} for scenario: {c_product}")

            print(f"\n============ CALCULATING {p.name} RISK ============\n")

            irp = [
                pct for pct in simulation.percentiles
                if pct.food == p
            ]
            if irp:
                irp = irp[0].percentile
            else:
                irp = Pmean

            print(f'Ingestion Rate Percentile for {p.name} = {irp}')

            risk_data = assess_risk(
                scenario=s, product=p, chemical=c,
                concentration=c_product,
                ingestion_percentile=irp,
                body_weight_percentile=bw
            )

            print(f"Calculated {p.name} risk values for scenario:")
            if risk_data:
                printvals(**risk_data)
            else:
                print('\tNone')

            simulation_breakdown[p.name.replace(' ', '_')] = {
                'concentration': c_product,
                'risk': risk_data
            }

        if with_breast_milk:
            print(f"\n======== CALCULATING {bm.name} CONCENTRATION ========\n")
            cumulative_ladd = 0
            # loop to computed cumulative LADD
            # that is used to estimate BM concentrations
            for product, result in simulation_breakdown.items():
                r = result['risk']
                life_risk = r['Lifetime']
                if life_risk:
                    cumulative_ladd += life_risk.get('intake', 0)

            c_fat, c_aq = calculate_c_product(
                scenario=s, product=bm, chemical=c,
                simulation=simulation,
                maternal_cumulative_ladd=cumulative_ladd
            )

            if c_fat is None or c_aq is None:
                c_total_bm = None
            else:
                f_mbm = s.parameters.for_media(bm).f_mbm.value or .5
                c_total_bm = (c_fat * f_mbm) + (c_aq * (1 - f_mbm))

            print(
                f"\nCalculated c_{bm.name} for scenario:"
                f" {c_fat} (fat) / {c_aq} (aqueous),"
                f" total = {c_total_bm}"
            )

            print(f"\n============ CALCULATING {bm.name} RISK ============\n")

            irp = [
                pct for pct in simulation.percentiles
                if pct.food == bm
            ]
            if irp:
                irp = irp[0].percentile
            else:
                irp = Pmean

            print(f'Ingestion Rate Percentile for {bm.name} = {irp}')

            risk_data = assess_risk(
                scenario=s, product=bm, chemical=c,
                concentration_fat=c_fat, concentration_aq=c_aq,
                ingestion_percentile=irp
            )

            print(f"Calculated {bm.name} risk values for scenario:")
            if risk_data:
                printvals(**risk_data)
            else:
                print('\tNone')

            simulation_breakdown[bm.name.replace(' ', '_')] = {
                'concentration': c_total_bm,
                'risk': risk_data
            }

        results.append(simulation_breakdown)

    print("\n================== SUMMARY OUTPUT ==================\n")

    for simulation_breakdown in results:
        for p, result in simulation_breakdown.items():
            c = result['concentration']
            r = result['risk']

            if r is None:
                ladd = None
                hq = None

            else:
                ladd = r['Lifetime']['intake']
                hq = r['Lifetime']['hazard_quotient']

            print(f'--- {p.title()} ---')
            print(f'\tConcentration = {c}')
            print(f'\tLADD = {ladd}')
            print(f'\tLADD HQ = {hq}\n')

    print("\n======================= DONE =======================\n")

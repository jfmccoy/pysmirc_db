import os
from contextlib import redirect_stdout

ROOT = os.path.abspath(os.path.dirname(__file__))

"""
This demo shows how you can load parameter data from Excel MIRC
into a new database using the functions in the data folder.
"""

DB_ROOT = os.path.dirname(ROOT)
PARAMS_FILES = [
    os.path.join(
        ROOT, 'static', '2 MIRC SMIRC Lookups-Equations.xlsm'
    ),
    os.path.join(
        ROOT, 'static', '1 TRIM.FaTE Output File Importer.xlsm'
    )
]


def create_db():
    print("\n================== INITIALIZING DB ==================\n")

    from .setup import make_db
    db = make_db()

    print("\n=================== MIGRATING DB ===================\n")

    db.migrate(os.path.join(DB_ROOT, 'migrations'))

    print("\n=================== LOADING DATA ===================\n")

    found_new = False

    print("Checking that builtins are present ...")
    from schema import Percentile
    if not Percentile.query.first():
        print("\tLoading builtins ...")
        from data import insert_default_builtins
        insert_default_builtins(db)
        found_new = True

    print("Checking that default scenarios are present ...")
    from schema import Scenario
    if not Scenario.query.first():
        from data import insert_default_scenarios
        print("\tLoading default scenarios ...")
        insert_default_scenarios(db)
        found_new = True

    from data import load_default_params
    for file in PARAMS_FILES:
        print(f"Checking for new parameters in {os.path.basename(file)} ...")
        with open(file, mode='rb') as f:
            # with open(os.devnull, 'w') as tout, redirect_stdout(tout):
            results = load_default_params(f, db)
            if results and not results[0].startswith('Unable'):
                found_new = True
            for txt in results:
                print(f'\t{txt}')

    if not found_new:
        print("\nDatabase already contains all required data.")

    print("\n================== VERIFYING DATA ==================\n")

    print(f'Percentile 1: {Percentile.query.first()}')

    print(f'Scenario 1: {Scenario.query.first()}')

    from schema import Chemical
    print(f'Chemical 1: {Chemical.query.first()}')

    print("\n======================= DONE =======================\n")
